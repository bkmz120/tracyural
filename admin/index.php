<?php 
include_once('include/inc.adm_header.php');
include_once('include/inc.nav.php');
?>

<h4>Заявки:</h4>

<table class="table table-hover">
	<tr>
		<td style="width: 30px; text-align: center;">#</td>
		<td style="width: 200px;">Дата</td>
		<td>Имя</td>
		<td style="width: 170px;">Телефон</td>
		<td>Email</td>
		<td style="width: 100px;">Форма</td>
		<td style="width: 100px;">Доп.</td>
		<td style="width: 300px;">UTM</td>
		<td style="width: 50px;">eml</td>
		<td style="width: 100px;">crm</td>
		<td style="width: 100px;">chimp</td>
	</tr>
	<?php
	$res = mysqli_query($DB, "SELECT * FROM ".DB_TABLE." ORDER BY id DESC");
	echo mysqli_error($DB);
	while ($row = mysqli_fetch_array($res))
	{
		$arRef = explode('?', $row['referer']);
		$arUtm = array(
			'referer' 			=> $arRef[0],
			'utm_source' 		=> $row['utm_source'],
			'utm_medium' 		=> $row['utm_medium'],
			'utm_campaign' 	=> $row['utm_campaign'],
			'utm_term' 		=> $row['utm_term'],
			'utm_content' 		=> $row['utm_content'],
		);
		$arUtm = array_map('mysql_prepare', $arUtm);
		
		$sent = mysql_prepare($row['sent']);
		$unisender = mysql_prepare($row['unisender']);

		if ($sent!='yes' && !empty($sent) && $sent!='no') {
			$sent = '<div title="'.mysql_prepare($row['sent']).'">error (?)</div>';
		}
		
		if ($unisender!='yes' && !empty($unisender)) {
			$unisender = '<div title="'.mysql_prepare($row['unisender']).'">(?)</div>';
		}
		
		echo '<tr>
			<td style="text-align: center;">'.$row['id'].'</td>
			<td>'.$row['datetime'].'</td>
			<td>'.mysql_prepare($row['name']).'</td>
			<td><a href="tel:'.mysql_prepare($row['phone']).'">'.mysql_prepare($row['phone']).'</a></td>
			<td><a href="mailto:'.mysql_prepare($row['email']).'">'.mysql_prepare($row['email']).'</a></td>
			<td>'.mysql_prepare($row['form']).'</td>
			<td>'.mysql_prepare($row['question']).'</td>
			<td>';
			foreach($arUtm AS $k => $v)
			{
				echo $k . ': ' . ((strlen($v) <= 0) ? 'нет' : $v) . '<br>';
			}
		echo '</td>
			<td>'.$sent.'</td>
			<td>'.mysql_prepare($row['crmid']).'</td>
			<td>'.$unisender.'</td>
		</tr>
		';
	}
	mysqli_free_result($res);
	?>
	</table>
</div>

<?php include_once('include/inc.adm_footer.php'); ?>