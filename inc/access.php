<?php
if (!isset($_SERVER['PHP_AUTH_USER']) OR ( isset($_GET['logout']) && $_GET['logout'] == 'Y'))
{
	Header ('WWW-Authenticate: Basic realm="Admin Page"');
	Header ('HTTP/1.0 401 Unauthorized');
	exit();
}
else {
	if ($_SERVER['PHP_AUTH_USER'] != ADMIN_NAME OR $_SERVER['PHP_AUTH_PW'] != ADMIN_PWD)
	{
		Header ('WWW-Authenticate: Basic realm="Admin Page"');
		Header ('HTTP/1.0 401 Unauthorized');
		exit();
	}
}