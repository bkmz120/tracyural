<?php include('inc/defines.php');

  //$last_edit = '?'.date('YmdHis');
  $last_edit = '?201708082427';

?><!DOCTYPE html>
<html lang="ru">
  <head>
    <meta charset="utf-8">
    <title><?php echo SITE_NAME; ?></title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="yandex-verification" content="cdda45af5e314464" />

    <link rel="shortcut icon" href="/images/favicon.ico">
    <link rel="stylesheet" href="/css/build2.css<?php echo $last_edit;?>">


    <meta name="description" content="Семинар человека, изменившего мир!" />
    <meta name="twitter:title" content="<?php echo SITE_NAME; ?>">
    <meta name="twitter:description" content="Семинар человека, изменившего мир!">
    <meta name="twitter:image" content="/share.png">

    <meta property="og:title" content="<?php echo SITE_NAME; ?>" />
    <meta property="og:type" content="article" />
    <meta property="og:url" content="/images/share.jpg" />
    <meta property="og:image" content="/images/share.jpg" />
    <meta property="og:description" content="Семинар человека, изменившего мир!" />
    <meta property="og:site_name" content="<?php echo SITE_NAME; ?>" />

  </head>

  <body>
    <header>
      <div class="container">
        <div class="header-logo"><img src="/images/header-logo.png"></div>
        <div class="header-right">
          <div class="header-date"><span>27</span>октября</div>
          <a href="tel:8-800-222-35-42" class="header-phone">8-800-222-35-42</a>
        </div>
      </div>
    </header>
    <div class="section" id="intro">
      <div class="container">
        <div class="mobile-date"><span>27</span>октября</div>
        <div class="intro-l1">Семинар человека,<br>изменившего мир!</div>
        <!-- <div class="intro-l2">Брайан<br><i></i>Трейси
          <span><i></i>в  Челябинске</span>
        </div> -->
        <img src="/images/header-title.png" class="intro-title">
        <div class="intro-strategy">
          <div class="intro-strategy-info">
            <div class="intro-strategy-title">СТРАТЕГИЯ ПРОРЫВА:</div>
            <div class="intro-strategy-row">Войди в <strong>20%</strong> людей, которые получают <strong>80%</strong> прибыли</div>
          </div>
          <a href="#intro-poopup" class="btn btn-yellow intro-strategy-btn open-popup">Успеть занять место</a>
        </div>

        <div class="intro-bottom">
          <div class="intro-bottom-items">
            <div class="intro-bottom-item">синхронный<br>перевод</div>
            <div class="intro-bottom-item"><strong>Конгресс Холл</strong>Челябинск, Ленина 35</div>
          </div>
          <div class="btn btn-yellow intro-bottom-btn openside">Получить билет</div>
        </div>
        <div class="intro-brian"><img src="/images/intro-brian.png"></div>
      </div>
    </div>
    <div class="section" id="numbers">
      <div class="container">
        <div class="numbers-items">
          <div class="numbers-item">
            <span class="numbers-item-num">300</span>
            <span class="numbers-item-desc">аудио и видеопрограмм<br>для обучения</span>
          </div><div class="numbers-item">
            <span class="numbers-item-num">86</span>
            <span class="numbers-item-desc">всемирно известных книг,<br>переведенных на 42 языка</span>
          </div><div class="numbers-item">
            <span class="numbers-item-num">5<span>млн.<br>чел.</span></span>
            <span class="numbers-item-desc">аудитория слушателей<br>семинаров</span>
          </div><div class="numbers-item">
            <span class="numbers-item-num">5 000</span>
            <span class="numbers-item-desc">проведенных лекций<br>и семинаров</span>
          </div><div class="numbers-item">
            <span class="numbers-item-num">3 300</span>
            <span class="numbers-item-desc">исследований среди<br>миллионеров</span>
          </div><div class="numbers-item">
            <span class="numbers-item-num">250<span>млн.<br>$</span></span>
            <span class="numbers-item-desc">фонд компании, которой<br>управлял Трейси</span>
          </div>
        </div>
      </div>
    </div>
    <div class="beleader forblur">
      <div class="container">
        <div class="beleader__title">всегда есть выбор:</div>
        <div class="beleader__lead">Правило Парето, которое действует<br> во всех сферах жизни: <strong>лидеры забирают 80%</strong>,<br> остальные конкурируют за <strong>оставшиеся 20%</strong></div>
        <div class="beleader__likeall">быть как все</div>
        <div class="beleader__likeleader">
          <div class="beleader__likeleader-title">стать лидером</div>
          <div class="beleader__likeleader-lead"><strong>забирать 80% рынка</strong> и ускорять достижение цели,<br>внедряя привычки успешных людей</div>
        </div>
      </div>
    </div>
    <div class="section" id="program">
      <div class="container">
        <div class="program-title-wr">
          <div class="program-title-top">
            <div class="program-title-top_line1">Всего лишь</div>
            <div class="program-title-top_line2">ОДНА ИДЕЯ БРАЙАНА ТРЕЙСИ</div>
          </div>
          <div class="program-title-bottom">
              может<strong> сэкономить 10 ЛЕТ</strong> на пути к цели!
              <div class="program-title-img"></div>
            </div>
        </div>

        <div class="program-lead">
          программа включает систему навыков, использование которой обеспечивает
          быстрый рост эффективности и управляемости личной и профессиональной жизни
        </div>

        <div class="program-items">
          <div class="program-item">
            Поймете,<br>
            что Вы хотите<br>
            на самом деле
          </div><div class="program-item">
            Составите свой<br>
            персональный<br>
            план действий
          </div><div class="program-item">
            Получите стремление<br>
            к развитию и улучшению<br>
            своей жизни
          </div><div class="program-item">
            Сможете наконец<br>
            сделать то, о чем<br>
            давно мечтали
          </div>
        </div>
      </div>
    </div>
    <div class="section" id="video">
      <div class="container container__wide">
        <div class="damn-border"><span class="db-top"></span><span class="db-left"></span><span class="db-right"></span><span class="db-bottom"></span></div>
        <div class="video-title">Получите видео Брайана Трейси</div>
        <div class="video-text">о том, как встроить мышление победителя и привлекать<br>в жизнь позитивные события. </div>
        <form class="custom-form video-form">
          <div class="form-inputs">
            <input type="text" name="email"  autocomplete="off" placeholder="Электронная почта"class="required" required="required">
            <input type="text" name="phone"  autocomplete="off" placeholder="Ваш телефон" class="required" required="required">
            <input type="hidden" name="form" value="Получить видео с Брайном Трейси">
            <input type="hidden" name="type" value="full">
            <input type="hidden" name="step" value="first">
            <input type="hidden" name="crm-prject" value="2">
          </div>
          <div class="form-btn">
            <div class="btn btn-yellow btn-send">Получить видео</div>
          </div>
        </form>
      </div>
    </div>
    <div class="section section_about" id="about">
      <div class="container">
        <div class="about-name">Брайан Трейси</div>
        <div class="about-best">Признанный лучший консультант В МИРЕ<br>по развитию личности и менедженту</div>
        <div class="about-target">
          <strong>Цель жизни Брайана - </strong>
          помочь вам достичь ваших личных и деловых целей<br>
          быстрее и легче, чем вы когда-либо себе представляли.
        </div>
        <div class="about-lists">
          <div class="damn-border"><span class="db-top"></span><span class="db-left"></span><span class="db-right"></span><span class="db-bottom"></span></div>
          <ul class="about-list">
            <li>До основания своей компании Brian Tracy International
              Брайан был главным операционным директором
              компании <strong>стоимостью 265 миллионов долларов.</strong></li>
            <li>Автор <strong>более 300 обучающих программ</strong>, бестселлер -
              «Психология дотижений», программа на 28 языках.</li>
            <li>Консультирует компании по стратегическому и операционному
              развитию с ценой решения <strong>более 250 миллиардов долларов</strong></li>
            <li>Побывал в путешествиях <strong>в 107 странах мира</strong>,
              на <strong>6 континентах</strong>, говорит на <strong>4 языках</strong>.</li>
          </ul>
          <ul class="about-list">
            <li>Проконсультировал более <strong>1000 компаний</strong>,
              и <strong>5 000 000 человек</strong>, более чем на <strong>5000 семинарах</strong></li>
            <li>Прошел путь от чернорабочего до Президента трех компаний,
              <strong>с филиалами в 23 странах Мира</strong></li>
            <li>Консультирует <strong>более 250 000 человек</strong>,
              в более чем 70 странах мира ежегодно.</li>
            <li>Счастливо женат, четверо детей</li>
          </ul>


        </div>
      </div>

      <div class="photos-slider photos-slider_about forblur">
        <div class="photos-slider__items owl-carousel owl-theme">
          <div class="photos-slider__item">
            <a href="images/tracy-gallery/680/1.jpg" data-lightbox="tracy-gallery" >
              <img src="images/tracy-gallery/330/1.jpg" class="photos-slider__item-img">
            </a>
          </div>
          <div class="photos-slider__item ">
            <a href="images/tracy-gallery/680/2.jpg" data-lightbox="tracy-gallery" >
              <img src="images/tracy-gallery/330/2.jpg" class="photos-slider__item-img">
            </a>
          </div>
          <div class="photos-slider__item">
            <a href="images/tracy-gallery/680/3.jpg" data-lightbox="tracy-gallery" >
              <img src="images/tracy-gallery/330/3.jpg" class="photos-slider__item-img">
            </a>
          </div>
          <div class="photos-slider__item">
            <a href="images/tracy-gallery/680/4.jpg" data-lightbox="tracy-gallery" >
              <img src="images/tracy-gallery/330/4.jpg" class="photos-slider__item-img">
            </a>
          </div>
          <div class="photos-slider__item">
            <a href="images/tracy-gallery/680/5.jpg" data-lightbox="tracy-gallery" >
              <img src="images/tracy-gallery/330/5.jpg" class="photos-slider__item-img">
            </a>
          </div>
          <div class="photos-slider__item">
            <a href="images/tracy-gallery/680/6.jpg" data-lightbox="tracy-gallery" >
              <img src="images/tracy-gallery/330/6.jpg" class="photos-slider__item-img">
            </a>
          </div>
          <div class="photos-slider__item">
            <a href="images/tracy-gallery/680/7.jpg" data-lightbox="tracy-gallery" >
              <img src="images/tracy-gallery/330/7.jpg" class="photos-slider__item-img">
            </a>
          </div>
          <div class="photos-slider__item">
            <a href="images/tracy-gallery/680/8.jpg" data-lightbox="tracy-gallery" >
              <img src="images/tracy-gallery/330/8.jpg" class="photos-slider__item-img">
            </a>
          </div>
          <div class="photos-slider__item">
            <a href="images/tracy-gallery/680/9.jpg" data-lightbox="tracy-gallery" >
              <img src="images/tracy-gallery/330/9.jpg" class="photos-slider__item-img">
            </a>
          </div>
          <div class="photos-slider__item">
            <a href="images/tracy-gallery/680/10.jpg" data-lightbox="tracy-gallery" >
              <img src="images/tracy-gallery/330/10.jpg" class="photos-slider__item-img">
            </a>
          </div>
          <div class="photos-slider__item">
            <a href="images/tracy-gallery/680/11.jpg" data-lightbox="tracy-gallery" >
              <img src="images/tracy-gallery/330/11.jpg" class="photos-slider__item-img">
            </a>
          </div>
        </div>
      </div>
    </div>


    <div class="section" id="books">
      <div class="container">
        <div class="section-title">Автор 86 бестселлеров</div>
        <div class="section-subtitle">Многие книги Брайана Трейси на тему эффективности, тайм-менеджмента, лидерства<br>и развития бизнеса, стали настольными книгами мотивационных бизнес-тренеров </div>
        <div class="bordered-box">
          <div class="bordered-box-title"><span>Книги</span></div>
          <div class="bordered-box-content bordered-box-content_book">
            <div class="books-slider owl-carousel owl-theme">
              <div class="books-slide">
                <div class="book-about"><strong>Выйди из зоны комфорта. Измени свою жизнь</strong>Книга №1 по саморазвитию, переведена на 40 языков, куплено более 1 200 000 экземпляров.</div>
                <img src="/images/books/01.jpg">
              </div><div class="books-slide">
                <div class="book-about"><strong>Достижение максимума.</strong>Книга вошла в список 50 классических книг о мотивации и лидерстве «50 Success Classics», помогает увеличить свои возможности для достижения максимального личного роста</div>
                <img src="/images/books/02.jpg">
              </div><div class="books-slide">
                <div class="book-about"><strong>21 секрет успеха миллионеров</strong>Принципы успеха для любого человека. Обобщен опыт сотен миллионеров и их привычек. Начальные условия не имеют значения.</div>
                <img src="/images/books/03.jpg">
              </div><div class="books-slide">
                <div class="book-about"><strong>Оставьте брезгливость, съеште лягушку</strong>Серия проверенных практикой приемов, помогающих эффективно планировать и организовывать рабочее время.</div>
                <img src="/images/books/04.jpg">
              </div><div class="books-slide">
                <div class="book-about"><strong>Нет оправданий. Сила самодисциплины.</strong>Каждая из 21 глав книги показывает, как повысить уровень дисциплинированности в каком-то одном аспекте жизни</div>
                <img src="/images/books/05.jpg">
              </div><div class="books-slide">
                <div class="book-about"><strong>Как нанять и удержать хороших сотрудников</strong>В книге обсуждаются важнейшие и проверенные на деле принципы (их 21) такой работы</div>
                <img src="/images/books/06.jpg">
              </div><div class="books-slide">
                <div class="book-about"><strong>Победа</strong>Секреты успеха. Выигрывают лишь те, кто имеет в своем распоряжении мощное интеллектуальное оружие, применяемое величайшими полководцами всех времен.</div>
                <img src="/images/books/07.jpg">
              </div><div class="books-slide">
                <div class="book-about"><strong>Технология достижений.</strong>Турбокоучинг по Брайану Трейси Книга учит предпринимателей справляться с любыми трудностями в быстро меняющейся бизнес-среде</div>
                <img src="/images/books/08.jpg">
              </div>
            </div>
          </div>

        </div>
        <div class="books-form">
          <form class="books-form__inner">
            <div class="books-form__img"></div>
            <div class="books-form__text">Зарегистрируйся сейчас и получи PDF-книгу Брайана Трейси "Достижение максимума"</div>
            <div class="books-form__items form-inputs">
              <input class="books-form__item books-form__item-first required" required="required" type="text" name="email" placeholder="Электронная почта">
              <input class="books-form__item required" required="required" type="text" name="phone" placeholder="Ваш телефон">
              <input type="hidden" name="form" value="Получить PDF-книгу">
              <input type="hidden" name="type" value="full">
              <input type="hidden" name="step" value="first">
              <input type="hidden" name="crm-prject" value="2">

            </div>
            <div class="books-form__btn ">
              <div class="books-form__btnitem btn btn-yellow btn-yellow-on-white btn-send">Скачать книгу</div>
            </div>
          </form>
        </div>

      </div>
    </div>

    <div class="section" id="trust">
      <div class="container">
        <div class="section-title">Брайану Трейси доверяют:</div>
        <div class="bordered-box">
          <div class="bordered-box-title"><span>Мировые компании</span></div>
          <div class="bordered-box-content">
            <div class="logos-items">
              <div class="logos-item">
                <img src="/images/trust1.png">
              </div><div class="logos-item">
                <img src="/images/trust2.png">
              </div><div class="logos-item">
                <img src="/images/trust3.png">
              </div><div class="logos-item">
                <img src="/images/trust4.png">
              </div><div class="logos-item">
                <img src="/images/trust5.png">
              </div>
            </div>
          </div>
        </div>
        <div class="bordered-box">
          <div class="bordered-box-title"><span>Российские Компании</span></div>
          <div class="bordered-box-content">
            <div class="logos-items">
              <div class="logos-item">
                <img src="/images/ruslogos/alfa.png">
              </div><div class="logos-item">
                <img src="/images/ruslogos/dixi.png">
              </div><div class="logos-item">
                <img src="/images/ruslogos/sberbank.png">
              </div><div class="logos-item">
                <img src="/images/ruslogos/sportmaster.png">
              </div><div class="logos-item">
                <img src="/images/ruslogos/uralsib.png">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="section" id="quotes">
      <div class="container" id="audio">
        <div class="section-title">Научись мыслить как миллионер!</div>
        <div class="quotes-items owl-carousel owl-theme">
          <div class="quotes-item q1">
            <div class="quotes-item-text">
              <span>Никто не лучше вас.<br>
              Никто не умнее вас. <br>
              Просто они начали раньше</span>
            </div>
            <div class="quotes-item-name">Брайан Трейси</div>
          </div>
          <div class="quotes-item q2">
            <div class="quotes-item-text">
              <span>Оставайтесь голодными.<br>
                Оставайтесь <br>
                безрассудными</span>
            </div>
            <div class="quotes-item-name">Стив Джобс</div>
          </div>
          <div class="quotes-item q3">
            <div class="quotes-item-text">
              <span>Знание – <br>
                главный инструмент <br>
                управления. </span>
            </div>
            <div class="quotes-item-name">Билл Гейтс</div>
          </div>
          <div class="quotes-item q4">
            <div class="quotes-item-text">
              <span>Особых талантов <br>
                у меня нет. Есть <br>
                огромное трудолюбие, <br>
                желание, упертость.</span>
            </div>
            <div class="quotes-item-name">Олег Тиньков</div>
          </div>
        </div>
      </div>
      <div class="container container__wide quotes-form-wrap" >
        <div class="damn-border"><span class="db-top"></span><span class="db-left"></span><span class="db-right"></span><span class="db-bottom"></span></div>
        <div class="quotes-form-title-wrap">
          <div class="quotes-form-title">
            <div class="quotes-form-title-line">Получи аудиокнигу Брайана Трейси</div>
            <div class="quotes-form-title-line">«Простые правила личного успеха»</div>
          </div>
        </div>
        <form class="custom-form quotes-form">
          <div class="form-inputs">
            <input type="text" name="email" autocomplete="off" placeholder="Электронная почта" class="required" required="required">
            <input type="text" name="phone" autocomplete="off" placeholder="Ваш телефон" class="required" required="required">
            <input type="hidden" name="form" value="Получить аудио-книгу">
            <input type="hidden" name="type" value="full">
            <input type="hidden" name="step" value="first">
            <input type="hidden" name="crm-prject" value="2">
          </div>
          <div class="form-btn">
            <div class="btn btn-yellow btn-send quotes-form__sendBtn">слушать аудио-книгу</div>
          </div>
        </form>
      </div>
    </div>

    <!-- Блок Видеоприглашения -->
    <div class="video-invit forblur" id="video-invite">
      <div class="container container__wide video-invit__container">
        <div class="video-invit__title">Видеоприглашения</div>
        <div class="video-invit__videos-wrap">
          <div class="player player_default-play video-invit__video">
            <div class="video-invit__video-poster js-video-invite-play-0">
              <img src="/images/video-invite/brian-poster.jpg" class="video-invit__video-poster-img">
              <div class="video-invit__video-poster-toner"></div>
              <div class="video-invit__video-poster-play"></div>
              <div class="video-invit__video-poster-text">Обращение Брайана Трейси</div>
            </div>
            <div data-type="youtube" data-video-id="LF7xeREEYBA"></div>
          </div>
          <div class="player player_default-play video-invit__video">
            <div class="video-invit__video-poster js-video-invite-play-1">
              <img src="/images/video-invite/kirill-poster.jpg" class="video-invit__video-poster-img">
              <div class="video-invit__video-poster-toner"></div>
              <div class="video-invit__video-poster-play"></div>
              <div class="video-invit__video-poster-text">Кирилл Липай, директор компании Malikspace</div>
            </div>
            <div data-type="youtube" data-video-id="zxhXj0dGXLo"></div>
          </div>
        </div>
      </div>
    </div>

    <!-- Блок про Сергея Озерова -->
    <div class="sj-oz forblur">
      <h1 class=" container sj-oz__title">Бонус</h1>
      <div class="sj-oz__subtietles">
      <div class="container sj-oz__subtietles-wrap">
        <h2 class="sj-oz__subtietle1">Сергей Озеров - </h2>
        <h3 class="sj-oz__subtietle2">Единственный персональный ученик Брайана Трейси в России</h3>
      </div>
      </div>
      <div class="container sj-oz__info">
      <h2 class="sj-oz__info-title">тема:</h2>
      <div class="sj-oz__info-item">«Как найти людей, которые помогут достичь цели.
                     Как взрастить в себе лидера и мотивировать цели»</div>
      </div>

      <div class="sj-oz__advantages">
      <div class="sj-oz__advantages-fon"></div>
      <div class="sj-oz__advantages-box">
        <img src="images/SOz/SOz.png" class="sj-oz__box-img">
          <div class="sj-oz__box-items-arrows">
            <div class="sj-oz__box-items-arrows-prev"></div>
            <div class="sj-oz__box-items-arrows-next"></div>
          </div>
        <div class="sj-oz__box-items ">
          <div class="sj-oz__box-item">

            <div class="sj-oz__box-item-text">
              <div class="sj-oz__box-item-num">
                <div class="sj-oz__box-item-num-cur">1</div>
                <div class="sj-oz__box-item-num-total">/ 7</div>
              </div>
              Лучший мотивационный спикер по мнению 2-х мировых авторитетов -
              <strong>Брайана Трейси и Джека Кэнфилда</strong> (бестселлер "Куриный бульон для души").
              Персональный ученик и друг Брайана и Джека
            </div>
          </div>
          <div class="sj-oz__box-item">
            <div class="sj-oz__box-item-text">
              <div class="sj-oz__box-item-num">
                <div class="sj-oz__box-item-num-cur">2</div>
                <div class="sj-oz__box-item-num-total">/ 7</div>
              </div>
              Бизнес эксперт, которому доверяют обучение своих сотрудников такие компании как:
              <strong>KPMG, Pfizer, Philip Morris, Heinz, Loreal, МегаФон, Газпромбанк, Банк Открытие,
              Банк Уралсиб</strong> и многие другие
            </div>
          </div>
          <div class="sj-oz__box-item">
            <div class="sj-oz__box-item-text">
              <div class="sj-oz__box-item-num">
                <div class="sj-oz__box-item-num-cur">3</div>
                <div class="sj-oz__box-item-num-total">/ 7</div>
              </div>
              Практик с <strong>20-ти летним опытом</strong>, прошедший путь от рядового торгового
              представителя до ТОП-менеджера в крупнейших мировых корпорациях
              (Philip Morris, Unilever, Citibank, Home Credit)
            </div>
          </div>
          <div class="sj-oz__box-item">
            <div class="sj-oz__box-item-text">
              <div class="sj-oz__box-item-num">
                <div class="sj-oz__box-item-num-cur">4</div>
                <div class="sj-oz__box-item-num-total">/ 7</div>
              </div>
              Успешный предприниматель - основатель и Генеральный <strong>директор компании
              «Practical Training School»</strong>
            </div>
          </div>
          <div class="sj-oz__box-item">
            <div class="sj-oz__box-item-text">
              <div class="sj-oz__box-item-num">
                <div class="sj-oz__box-item-num-cur">5</div>
                <div class="sj-oz__box-item-num-total">/ 7</div>
              </div>
              <strong>Профессионал в 3-х сферах</strong> — продажи и переговоры, эффективный менеджмент, мотивация и личная эффективность
            </div>
          </div>
          <div class="sj-oz__box-item">
            <div class="sj-oz__box-item-text">
              <div class="sj-oz__box-item-num">
                <div class="sj-oz__box-item-num-cur">6</div>
                <div class="sj-oz__box-item-num-total">/ 7</div>
              </div>
              Более <strong>300 проведенных мероприятий</strong> (тренингов, семинаров, мастер-классов)
            </div>
          </div>
          <div class="sj-oz__box-item">
            <div class="sj-oz__box-item-text">
              <div class="sj-oz__box-item-num">
                <div class="sj-oz__box-item-num-cur">7</div>
                <div class="sj-oz__box-item-num-total">/ 7</div>
              </div>
              Более 15000 учеников, прошедших тренинги Сергея
              и <strong>уже добившихся ощутимых перемен в своей жизни</strong>
            </div>
          </div>
        </div>
      </div>
      </div>

      <div class="books-form books-form_sjoz">
        <form class="books-form__inner books-form__inner_sjoz">
          <div class="books-form__title books-form__title_sjoz">Секреты мотивации от Сергея Озерова</div>
          <div class="books-form__text books-form__text_sjoz">зарегистрируйся сейчас и смотри видео-интервью с Сергеем Озеровым о том, как мотивировать себя на достижение цели</div>
          <div class="books-form__items books-form__items_sjoz form-inputs">
            <input class="books-form__item books-form__item-first books-form__item-first_sjoz required" required="required" type="text" name="email" placeholder="Электронная почта">
            <input class="books-form__item required" required="required" type="text" name="phone" placeholder="Ваш телефон">
            <input type="hidden" name="form" value="Секреты мотивации от Сергея Озерова">
            <input type="hidden" name="type" value="full">
            <input type="hidden" name="step" value="first">
            <input type="hidden" name="crm-prject" value="2">
          </div>
          <div class="books-form__btn books-form__btn_sjoz">
            <div class="books-form__btnitem btn btn-yellow btn-yellow-on-white btn-send">Получить видео</div>
          </div>
        </form>
      </div>
    </div>


    <!-- Блок Кому и чем полезен семинар   -->
    <div class="forwho  forblur"  id="forwho">
      <div class="forwho__title">кому и чем полезен семинар</div>

      <div class="forwho__profession">

      <div class="forwho__item">
        <div class="forwho__profession-img forwho__profession-img_1"></div>
        <div class="forwho__profession-info">Предприниматели и владельцы бизнеса</div>
        <div class="forwho__mobile">
          <div class="forwho__mobile-arrow"></div>
          <div class="forwho__mobile-text">
            <ul class="forwho__mobile-list">
              <li class="forwho__mobile-list-item">Получите план-стратегию личностного роста и заряд вдохновляющей мотивации для сотрудников.</li>
              <li class="forwho__mobile-list-item">Научитесь получать удовольствие от  процесса достижения целей, быть счастливым здесь и сейчас.</li>
              <li class="forwho__mobile-list-item">Поймете, как масштабировать бизнес, меняя мышление и внедряя привычки миллионеров.</li>
              <li class="forwho__mobile-list-item">Посетите событие года в Челябинске с рекордной концентрацией людей, заинтересованных в развитии и новых деловых связях.</li>
              <li class="forwho__mobile-list-item">Вдохновитесь живой энергетикой человека-легенды.</li>
            </ul>
            <h3 class="forwho__mobile-subtitle">Удвойте пользу от семинара, посещая его с вашими сотрудниками</h3>
            <p class="forwho__mobile-text-p">Тренер миллионеров научит сотрудников постоянно развиваться, повышать профессионализм, фокусироваться на результате и увеличивать прибыль компании.</p>
            <a href="#for-who-mobile-popup" for-who-type="predprinimateli" class="forwho__mobile-btn open-popup">Получить билет</a>

          </div>
        </div>
        <div class="forwho__box-item-border">
          <div class="forwho__box-item-border-top"></div>
          <div class="forwho__box-item-border-right"></div>
          <div class="forwho__box-item-border-bottom"></div>
          <div class="forwho__box-item-border-left"></div>
        </div>
        <div class="forwho__hover">
          <div  class="forwho__hover-text">
            <ul class="forwho__hover-list">
              <li class="forwho__hover-list-item">Получите план-стратегию личностного роста и заряд вдохновляющей мотивации для сотрудников.</li>
              <li class="forwho__hover-list-item">Научитесь получать удовольствие от  процесса достижения целей, быть счастливым здесь и сейчас.</li>
              <li class="forwho__hover-list-item">Поймете, как масштабировать бизнес, меняя мышление и внедряя привычки...</li>
            </ul>
          </div>
          <a class="forwho__hover-link open-popup" href="#forwho-full-text-1">Подробнее...</a>
        </div>
        <div id="forwho-full-text-1" class="forwho__item-popup">
          <div class="forwho__item-popup-title">Предприниматели и владельцы бизнеса</div>
          <div class="forwho__item-popup-text">
            <ul class="forwho__item-popup-list">
              <li class="forwho__item-popup-list-item">Получите план-стратегию личностного роста и заряд вдохновляющей мотивации для сотрудников.</li>
              <li class="forwho__item-popup-list-item">Научитесь получать удовольствие от  процесса достижения целей, быть счастливым здесь и сейчас.</li>
              <li class="forwho__item-popup-list-item">Поймете, как масштабировать бизнес, меняя мышление и внедряя привычки миллионеров.</li>
              <li class="forwho__item-popup-list-item">Посетите событие года в Челябинске с рекордной концентрацией людей, заинтересованных в развитии и новых деловых связях.</li>
              <li class="forwho__item-popup-list-item">Вдохновитесь живой энергетикой человека-легенды.</li>
            </ul>
            <h3 class="forwho__item-popup-subtitle">Удвойте пользу от семинара, посещая его с вашими сотрудниками</h3>
            <p class="forwho__item-popup-text-p">Тренер миллионеров научит сотрудников постоянно развиваться, повышать профессионализм, фокусироваться на результате и увеличивать прибыль компании.</p>
          </div>
          <div class="forwho__item-popup-form">
            <div class="forwho__item-popup-form-info">Заполните поля «Имя» / «Телефон» / «E-mail»</div>
            <form class="forwho__item-popup-form-form">
              <div class="form-inputs">
                <input type="text" name="name" autocomplete="off" placeholder="Ваше имя" data-err="Введите имя!" class="forwho__item-popup-form-inp required" required="required">
                <input type="text" name="phone" autocomplete="off" placeholder="Номер телефона" data-err="Введите номер!" class="forwho__item-popup-form-inp required" required="required">
                <input type="text" name="email" autocomplete="off" placeholder="Ваш e-mail" data-err="Введите e-mail!" class="forwho__item-popup-form-inp required" required="required">
              </div>

              <input type="hidden" name="form" value="Форма для кого Предприниматели и владельцы бизнеса">
              <input type="hidden" name="question" value="">
              <input type="hidden" name="step" value="first">
              <input type="hidden" name="type" value="full">
              <input type="hidden" name="crm-prject" value="1">
              <input type="hidden" name="id">
              <div class="forwho__item-popup-form-btn-wr">
                <div class="btn btn-send btn-yellow  forwho__item-popup-form-send-btn">Получить билет</div>
              </div>
            </form>
          </div>
        </div>
      </div>

      <div class="forwho__item">
        <div class="forwho__profession-img forwho__profession-img_2"></div>
        <div class="forwho__profession-info">Топ-менеджеры</div>
        <div class="forwho__mobile">
          <div class="forwho__mobile-arrow"></div>
          <div class="forwho__mobile-text">
            <ul class="forwho__mobile-list">
              <li class="forwho__mobile-list-item">Получите инструменты самоменеджмента и мотивации, собранные за 20 лет исследований полезных привычек 3300 миллионеров.</li>
              <li class="forwho__mobile-list-item">Поймете, как стать максимально эффективным и незаменимым руководителем с растущими финансовыми показателями.</li>
              <li class="forwho__mobile-list-item">Сэкономите до 10 лет на пути к цели, встраивая привычки успешных людей и понимая как вдохновлять себя и других.</li>
            </ul>
            <h3 class="forwho__mobile-subtitle">Удвойте пользу от семинара, посещая его с подчиненными</h3>
            <p class="forwho__mobile-text-p">Искорените лень и низкую эффективность сотрудников, синтезируя в них амбиции и продуктивность.</p>
            <a href="#for-who-mobile-popup" for-who-type="top-managers" class="forwho__mobile-btn open-popup">Получить билет</a>
          </div>
        </div>
        <div class="forwho__box-item-border">
          <div class="forwho__box-item-border-top"></div>
          <div class="forwho__box-item-border-right"></div>
          <div class="forwho__box-item-border-bottom"></div>
          <div class="forwho__box-item-border-left"></div>
        </div>
        <div class="forwho__hover">
          <div class="forwho__hover-text">
            <ul class="forwho__hover-list">
              <li class="forwho__hover-list-item">Получите инструменты самоменеджмента и мотивации, собранные за 20 лет исследований полезных привычек 3300 миллионеров.</li>
              <li class="forwho__hover-list-item">Поймете, как стать максимально эффективным и незаменимым руководителем с растущими финансовыми показателями.</li>
              <li class="forwho__hover-list-item">Сэкономите до 10 лет на пути к цели, встраивая...</li>
            </ul>
          </div>
          <a class="forwho__hover-link open-popup" href="#forwho-full-text-2">Подробнее...</a>
        </div>
        <div id="forwho-full-text-2" class="forwho__item-popup">
          <div class="forwho__item-popup-title">Топ-менеджеры</div>
          <div class="forwho__item-popup-text">
            <ul class="forwho__item-popup-list">
              <li class="forwho__item-popup-list-item">Получите инструменты самоменеджмента и мотивации, собранные за 20 лет исследований полезных привычек 3300 миллионеров.</li>
              <li class="forwho__item-popup-list-item">Поймете, как стать максимально эффективным и незаменимым руководителем с растущими финансовыми показателями.</li>
              <li class="forwho__item-popup-list-item">Сэкономите до 10 лет на пути к цели, встраивая привычки успешных людей и понимая как вдохновлять себя и других.</li>
            </ul>
            <h3 class="forwho__item-popup-subtitle">Удвойте пользу от семинара, посещая его с подчиненными</h3>
            <p class="forwho__item-popup-text-p">Искорените лень и низкую эффективность сотрудников, синтезируя в них амбиции и продуктивность.</p>
          </div>
          <div class="forwho__item-popup-form">
            <div class="forwho__item-popup-form-info">Заполните поля «Имя» / «Телефон» / «E-mail»</div>
            <form class="forwho__item-popup-form-form">
              <div class="form-inputs">
                <input type="text" name="name" autocomplete="off" placeholder="Ваше имя" data-err="Введите имя!" class="forwho__item-popup-form-inp required" required="required">
                <input type="text" name="phone" autocomplete="off" placeholder="Номер телефона" data-err="Введите номер!" class="forwho__item-popup-form-inp required" required="required">
                <input type="text" name="email" autocomplete="off" placeholder="Ваш e-mail" data-err="Введите e-mail!" class="forwho__item-popup-form-inp required" required="required">
              </div>

              <input type="hidden" name="form" value="Форма для кого Топ-менеджеры">
              <input type="hidden" name="question" value="">
              <input type="hidden" name="step" value="first">
              <input type="hidden" name="type" value="full">
              <input type="hidden" name="crm-prject" value="1">
              <input type="hidden" name="id">
              <div class="forwho__item-popup-form-btn-wr">
                <div class="btn btn-send btn-yellow  forwho__item-popup-form-send-btn">Получить билет</div>
              </div>
            </form>
          </div>
        </div>
      </div>

      <div class="forwho__item">
        <div class="forwho__profession-img forwho__profession-img_3"></div>
        <div class="forwho__profession-info">Менеджеры</div>
        <div class="forwho__mobile">
          <div class="forwho__mobile-arrow"></div>
          <div class="forwho__mobile-text">
            <ul class="forwho__mobile-list">
              <li class="forwho__mobile-list-item">Узнаете секрет продуктивности миллионеров, получите мотивацию и найдете энергию для стремительного достижения важных для вас целей.</li>
              <li class="forwho__mobile-list-item">Поймёте, как отличать точные целевые действия от “пустых”, как стать теми 20%, которые делают 80% результата и забирают 80% прибыли.</li>
              <li class="forwho__mobile-list-item">Ясно увидите, как стать по-настоящему успешным человеком и получить навыки, которые гарантируют Вам устойчивую позицию в Вашей должности, востребованность на рынке труда, карьерный и финансовый рост.</li>
            </ul>
            <a href="#for-who-mobile-popup" for-who-type="managers" class="forwho__mobile-btn open-popup">Получить билет</a>
          </div>
        </div>
        <div class="forwho__box-item-border">
          <div class="forwho__box-item-border-top"></div>
          <div class="forwho__box-item-border-right"></div>
          <div class="forwho__box-item-border-bottom"></div>
          <div class="forwho__box-item-border-left"></div>
        </div>
        <div class="forwho__hover">
          <div class="forwho__hover-text">
            <ul class="forwho__hover-list">
              <li class="forwho__hover-list-item">Узнаете секрет продуктивности миллионеров, получите мотивацию и найдете энергию для стремительного достижения важных для вас целей.</li>
              <li class="forwho__hover-list-item">Поймёте, как отличать точные целевые действия от “пустых”, как стать теми 20%, которые делают 80% результата и забирают 80% прибыли.</li>
              <li class="forwho__hover-list-item">Ясно увидите, как...</li>
            </ul>
          </div>
          <a class="forwho__hover-link open-popup" href="#forwho-full-text-3">Подробнее...</a>
        </div>
        <div id="forwho-full-text-3" class="forwho__item-popup">
          <div class="forwho__item-popup-title">Менеджеры</div>
          <div class="forwho__item-popup-text">
            <ul class="forwho__item-popup-list">
              <li class="forwho__item-popup-list-item">Узнаете секрет продуктивности миллионеров, получите мотивацию и найдете энергию для стремительного достижения важных для вас целей.</li>
              <li class="forwho__item-popup-list-item">Поймёте, как отличать точные целевые действия от “пустых”, как стать теми 20%, которые делают 80% результата и забирают 80% прибыли.</li>
              <li class="forwho__item-popup-list-item">Ясно увидите, как стать по-настоящему успешным человеком и получить навыки, которые гарантируют Вам устойчивую позицию в Вашей должности, востребованность на рынке труда, карьерный и финансовый рост.</li>
            </ul>
          </div>
          <div class="forwho__item-popup-form">
            <div class="forwho__item-popup-form-info">Заполните поля «Имя» / «Телефон» / «E-mail»</div>
            <form class="forwho__item-popup-form-form">
              <div class="form-inputs">
                <input type="text" name="name" autocomplete="off" placeholder="Ваше имя" data-err="Введите имя!" class="forwho__item-popup-form-inp required" required="required">
                <input type="text" name="phone" autocomplete="off" placeholder="Номер телефона" data-err="Введите номер!" class="forwho__item-popup-form-inp required" required="required">
                <input type="text" name="email" autocomplete="off" placeholder="Ваш e-mail" data-err="Введите e-mail!" class="forwho__item-popup-form-inp required" required="required">
              </div>

              <input type="hidden" name="form" value="Форма для кого Менеджеры">
              <input type="hidden" name="question" value="">
              <input type="hidden" name="step" value="first">
              <input type="hidden" name="type" value="full">
              <input type="hidden" name="crm-prject" value="1">
              <input type="hidden" name="id">
              <div class="forwho__item-popup-form-btn-wr">
                <div class="btn btn-send btn-yellow  forwho__item-popup-form-send-btn">Получить билет</div>
              </div>
            </form>
          </div>
        </div>
      </div>

      <div class="forwho__item">
        <div class="forwho__profession-img forwho__profession-img_4"></div>
        <div class="forwho__profession-info">Фрилансеры</div>
        <div class="forwho__mobile">
          <div class="forwho__mobile-arrow"></div>
          <div class="forwho__mobile-text">
            <ul class="forwho__mobile-list">
              <li class="forwho__mobile-list-item">Поймете, как повысить ценность Вашей работы, увеличить уровень заработка и обеспечить себя постоянными клиентами, оставаясь свободными.</li>
              <li class="forwho__mobile-list-item">Поймете как сочетать счастливую жизнь и продуктивную работу.</li>
              <li class="forwho__mobile-list-item">Узнаете как развить навык сдавать проекты в срок, одновременно выполняя несколько задач.</li>
              <li class="forwho__mobile-list-item">Поймете, что для вас значит навык лидерства, как собрать команду и управлять группами экспертов.</li>
              <li class="forwho__mobile-list-item">Зарядитесь энергетикой тренера миллионеров и сможете найти партнеров в среде бизнес-элиты города.</li>
            </ul>
            <a href="#for-who-mobile-popup" for-who-type="freelancers" class="forwho__mobile-btn open-popup">Получить билет</a>
          </div>
        </div>
        <div class="forwho__box-item-border">
          <div class="forwho__box-item-border-top"></div>
          <div class="forwho__box-item-border-right"></div>
          <div class="forwho__box-item-border-bottom"></div>
          <div class="forwho__box-item-border-left"></div>
        </div>
        <div class="forwho__hover">
          <div class="forwho__hover-text">
            <ul class="forwho__hover-list">
              <li class="forwho__hover-list-item">Поймете, как повысить ценность Вашей работы, увеличить уровень заработка и обеспечить себя постоянными клиентами, оставаясь свободными.</li>
              <li class="forwho__hover-list-item">Поймете как сочетать счастливую жизнь и продуктивную работу.</li>
              <li class="forwho__hover-list-item">Узнаете как развить навык сдавать проекты в срок, одновременно выполняя...</li>
            </ul>
          </div>
          <a class="forwho__hover-link open-popup" href="#forwho-full-text-4">Подробнее...</a>
        </div>
        <div id="forwho-full-text-4" class="forwho__item-popup">
          <div class="forwho__item-popup-title">фрилансеры</div>
          <div class="forwho__item-popup-text">
            <ul class="forwho__item-popup-list">
              <li class="forwho__item-popup-list-item">Поймете, как повысить ценность Вашей работы, увеличить уровень заработка и обеспечить себя постоянными клиентами, оставаясь свободными.</li>
              <li class="forwho__item-popup-list-item">Поймете как сочетать счастливую жизнь и продуктивную работу.</li>
              <li class="forwho__item-popup-list-item">Узнаете как развить навык сдавать проекты в срок, одновременно выполняя несколько задач.</li>
              <li class="forwho__item-popup-list-item">Поймете, что для вас значит навык лидерства, как собрать команду и управлять группами экспертов.</li>
              <li class="forwho__item-popup-list-item">Зарядитесь энергетикой тренера миллионеров и сможете найти партнеров в среде бизнес-элиты города.</li>
            </ul>
          </div>
          <div class="forwho__item-popup-form">
            <div class="forwho__item-popup-form-info">Заполните поля «Имя» / «Телефон» / «E-mail»</div>
            <form class="forwho__item-popup-form-form">
              <div class="form-inputs">
                <input type="text" name="name" autocomplete="off" placeholder="Ваше имя" data-err="Введите имя!" class="forwho__item-popup-form-inp required" required="required">
                <input type="text" name="phone" autocomplete="off" placeholder="Номер телефона" data-err="Введите номер!" class="forwho__item-popup-form-inp required" required="required">
                <input type="text" name="email" autocomplete="off" placeholder="Ваш e-mail" data-err="Введите e-mail!" class="forwho__item-popup-form-inp required" required="required">
              </div>

              <input type="hidden" name="form" value="Форма для кого Фрилансеры">
              <input type="hidden" name="question" value="">
              <input type="hidden" name="step" value="first">
              <input type="hidden" name="type" value="full">
              <input type="hidden" name="crm-prject" value="1">
              <input type="hidden" name="id">
              <div class="forwho__item-popup-form-btn-wr">
                <div class="btn btn-send btn-yellow  forwho__item-popup-form-send-btn">Получить билет</div>
              </div>
            </form>
          </div>
        </div>
      </div>

      <div class="forwho__item">
        <div class="forwho__profession-img forwho__profession-img_5"></div>
        <div class="forwho__profession-info">Бизнес-тренеры</div>
        <div class="forwho__mobile">
          <div class="forwho__mobile-arrow"></div>
          <div class="forwho__mobile-text">
            <ul class="forwho__mobile-list">
              <li class="forwho__mobile-list-item">Оцените в деле одного из лучших бизнес-тренеров мира, основателя школы бизнес-консультантов с более 5000 выпускников.</li>
              <li class="forwho__mobile-list-item">Возьмете на вооружение технологию целеполагания и мотивации, проверенную на 3300 миллионерах</li>
              <li class="forwho__mobile-list-item">Проявите свою экспертность в окружении людей, которые увлекаются психологией саморазвития, селф-менеджментом, ищут пути масштабирования бизнеса и могут стать вашими клиентами.</li>
              <li class="forwho__mobile-list-item">За 6 часов сможете из первых уст услышать лучшие идеи Брайна Трейси и принципы мышления успешных людей.</li>
              <li class="forwho__mobile-list-item">Лично познакомитесь с экспертом, труды которого легли в основу большинства тренингов личностного роста в мире. </li>
            </ul>
            <a href="#for-who-mobile-popup" for-who-type="bussines-trainers" class="forwho__mobile-btn open-popup">Получить билет</a>
          </div>
        </div>
        <div class="forwho__box-item-border">
          <div class="forwho__box-item-border-top"></div>
          <div class="forwho__box-item-border-right"></div>
          <div class="forwho__box-item-border-bottom"></div>
          <div class="forwho__box-item-border-left"></div>
        </div>
        <div class="forwho__hover">
          <div class="forwho__hover-text">
            <ul class="forwho__hover-list">
              <li class="forwho__hover-list-item">Оцените в деле одного из лучших бизнес-тренеров мира, основателя школы бизнес-консультантов с более 5000 выпускников.</li>
              <li class="forwho__hover-list-item">Возьмете на вооружение технологию целеполагания и мотивации, проверенную на 3300 миллионерах</li>
              <li class="forwho__hover-list-item">Проявите свою экспертность в окружении людей, которые...</li>
            </ul>
          </div>
          <a class="forwho__hover-link open-popup" href="#forwho-full-text-5">Подробнее...</a>
        </div>
        <div id="forwho-full-text-5" class="forwho__item-popup">
          <div class="forwho__item-popup-title">Бизнес-тренеры</div>
          <div class="forwho__item-popup-text">
            <ul class="forwho__item-popup-list">
              <li class="forwho__item-popup-list-item">Оцените в деле одного из лучших бизнес-тренеров мира, основателя школы бизнес-консультантов с более 5000 выпускников.</li>
              <li class="forwho__item-popup-list-item">Возьмете на вооружение технологию целеполагания и мотивации, проверенную на 3300 миллионерах</li>
              <li class="forwho__item-popup-list-item">Проявите свою экспертность в окружении людей, которые увлекаются психологией саморазвития, селф-менеджментом, ищут пути масштабирования бизнеса и могут стать вашими клиентами.</li>
              <li class="forwho__item-popup-list-item">За 6 часов сможете из первых уст услышать лучшие идеи Брайна Трейси и принципы мышления успешных людей.</li>
              <li class="forwho__item-popup-list-item">Лично познакомитесь с экспертом, труды которого легли в основу большинства тренингов личностного роста в мире. </li>
            </ul>
          </div>
          <div class="forwho__item-popup-form">
            <div class="forwho__item-popup-form-info">Заполните поля «Имя» / «Телефон» / «E-mail»</div>
            <form class="forwho__item-popup-form-form">
              <div class="form-inputs">
                <input type="text" name="name" autocomplete="off" placeholder="Ваше имя" data-err="Введите имя!" class="forwho__item-popup-form-inp required" required="required">
                <input type="text" name="phone" autocomplete="off" placeholder="Номер телефона" data-err="Введите номер!" class="forwho__item-popup-form-inp required" required="required">
                <input type="text" name="email" autocomplete="off" placeholder="Ваш e-mail" data-err="Введите e-mail!" class="forwho__item-popup-form-inp required" required="required">
              </div>

              <input type="hidden" name="form" value="Форма для кого Бизнес-тренеры">
              <input type="hidden" name="question" value="">
              <input type="hidden" name="step" value="first">
              <input type="hidden" name="type" value="full">
              <input type="hidden" name="crm-prject" value="1">
              <input type="hidden" name="id">
              <div class="forwho__item-popup-form-btn-wr">
                <div class="btn btn-send btn-yellow  forwho__item-popup-form-send-btn">Получить билет</div>
              </div>
            </form>
          </div>
        </div>
      </div>

      <div class="forwho__item">
        <div class="forwho__profession-img forwho__profession-img_6"></div>
        <div class="forwho__profession-info forwho__profession-info_location">Все, кто хочет достигать максимума</div>
        <div class="forwho__mobile">
          <div class="forwho__mobile-arrow forwho__mobile-arrow_gold"></div>
          <div class="forwho__mobile-text">
            <ul class="forwho__mobile-list">
              <li class="forwho__mobile-list-item">Получите инструкцию по достижению любых целей, проверенную на 3300 миллионерах, самостоятельно создавших свое состояние. </li>
              <li class="forwho__mobile-list-item">Поймете, как войти в 20% людей отрасли, которые получают 80% прибыли. </li>
              <li class="forwho__mobile-list-item">Получите вдохновляющую энергию от одного из лучших мотивационных спикеров мирового уровня.</li>
              <li class="forwho__mobile-list-item">Получите навыки мышления успешных людей и заряд самомотивации на несколько лет вперед</li>
            </ul>
            <a href="#for-who-mobile-popup" for-who-type="success-peoples" class="forwho__mobile-btn open-popup">Получить билет</a>
          </div>
        </div>
        <div class="forwho__box-item-border">
          <div class="forwho__box-item-border-top"></div>
          <div class="forwho__box-item-border-right"></div>
          <div class="forwho__box-item-border-bottom"></div>
          <div class="forwho__box-item-border-left"></div>
        </div>
        <div class="forwho__hover">
          <div class="forwho__hover-text">
            <ul class="forwho__hover-list">
              <li class="forwho__hover-list-item">Получите инструкцию по достижению любых целей, проверенную на 3300 миллионерах, самостоятельно создавших свое состояние. </li>
              <li class="forwho__hover-list-item">Поймете, как войти в 20% людей отрасли, которые получают 80% прибыли. </li>
              <li class="forwho__hover-list-item">Получите вдохновляющую энергию от одного из лучших мотивационных спикеров...</li>
            </ul>
          </div>
          <a class="forwho__hover-link open-popup" href="#forwho-full-text-6">Подробнее...</a>
        </div>
        <div id="forwho-full-text-6" class="forwho__item-popup">
          <div class="forwho__item-popup-title">Все, кто хочет достигать максимума</div>
          <div class="forwho__item-popup-text">
            <ul class="forwho__item-popup-list">
              <li class="forwho__item-popup-list-item">Получите инструкцию по достижению любых целей, проверенную на 3300 миллионерах, самостоятельно создавших свое состояние. </li>
              <li class="forwho__item-popup-list-item">Поймете, как войти в 20% людей отрасли, которые получают 80% прибыли. </li>
              <li class="forwho__item-popup-list-item">Получите вдохновляющую энергию от одного из лучших мотивационных спикеров мирового уровня.</li>
              <li class="forwho__item-popup-list-item">Получите навыки мышления успешных людей и заряд самомотивации на несколько лет вперед</li>
            </ul>
          </div>
          <div class="forwho__item-popup-form">
            <div class="forwho__item-popup-form-info">Заполните поля «Имя» / «Телефон» / «E-mail»</div>
            <form class="forwho__item-popup-form-form">
              <div class="form-inputs">
                <input type="text" name="name" autocomplete="off" placeholder="Ваше имя" data-err="Введите имя!" class="forwho__item-popup-form-inp required" required="required">
                <input type="text" name="phone" autocomplete="off" placeholder="Номер телефона" data-err="Введите номер!" class="forwho__item-popup-form-inp required" required="required">
                <input type="text" name="email" autocomplete="off" placeholder="Ваш e-mail" data-err="Введите e-mail!" class="forwho__item-popup-form-inp required" required="required">
              </div>

              <input type="hidden" name="form" value="Форма для кого Все кто хочет достигать максимума">
              <input type="hidden" name="question" value="">
              <input type="hidden" name="step" value="first">
              <input type="hidden" name="type" value="full">
              <input type="hidden" name="crm-prject" value="1">
              <input type="hidden" name="id">
              <div class="forwho__item-popup-form-btn-wr">
                <div class="btn btn-send btn-yellow  forwho__item-popup-form-send-btn">Получить билет</div>
              </div>
            </form>
          </div>
        </div>
      </div>

      </div>
    </div>


    <!-- Блок Networking  -->

    <div class="networking  forblur" id="networking">
      <div class="networking__title">
        <div class="networking__title-items">
          <h1 class="networking__main">Networking</h1>
          <h2 class="networking__subtitle ">2 формата:</h2>
        </div>
        <div class="networking__about">
          <div class="networking__about-frame"></div>
          <a href="#networking-full-about" class="networking__about-link open-popup" >
            <div class="networking__about-link-text networking__about-link-text_first">Что такое</div>
            <div class="networking__about-link-text">нетворкинг?</div>
          </a>
          <div id="networking-full-about" class="networking__full-about">
            <h3>Что такое нетворкинг?</h3>
            <p>– это марафон полезных деловых знакомств.</p>
            <br>
            <p>Большинство самых успешных бизнесменов соглашаются с принципом: «Неважно, насколько вы умны, важно, с кем вы знакомы». </p>
            <br>
            <p><b>Нетворкинг – это:</b></p>
            <ul class="networking__full-about-list">
              <li class="networking__full-about-list-item">Новые клиенты</li>
              <li class="networking__full-about-list-item">Новые партнеры</li>
              <li class="networking__full-about-list-item">Опытные наставники</li>
              <li class="networking__full-about-list-item">Верные друзья</li>
            </ul>
            <br>
            <p><b>! Не забудьте взять с собой визитки и подготовьте краткую самопрезентацию,</b> чтобы за пару минут «продать» собеседнику свою идею/предложение и убедить сохранить ваш контакт.</p>
            <div class="center-button">
              <a href="#networkform-poopup" networking-type="whatisit" class="btn btn-yellow networking__full-about-btn open-popup">Оставить заявку</a>
            </div>
          </div>
        </div>
      </div>


      <div class="networking__category">
        <div class="networking__category-item networking__category-item_first">
          <div class="networking__category-title">
            <div class="networking__category-title-main networking__category-title-main_vip">vip</div>
            <div class="networking__category-title-subtitle networking__category-title-subtitle_vip">нетворкинг</div>
          </div>
          <div class="networking__info">
            <div class="networking__item">До 50 участников</div>
            <div class="networking__item">Бизнес-элита города</div>
            <div class="networking__item networking__item_first">Серьёзные партнерские отношения</div>
          </div>
          <div class="networking__bottom networking__bottom_first">
            <a class="networking__btn networking__btn_first open-popup"  networking-type="vip" href="#networkform-poopup">
              Оставить заявку
            </a>
            <div class="networking__bottom-info">Входит в форматы <strong>ВИП и ПЛАТИНУМ</strong></div>
          </div>

        </div>

        <div class="networking__category-item networking__category-item_second">
          <div class="networking__category-title networking__category-title_second">
            <div class="networking__category-title-main networking__category-title-main_business">business</div>
            <div class="networking__category-title-subtitle networking__category-title-subtitle_business">нетворкинг</div>
          </div>
          <div class="networking__info">
            <div class="networking__item">Более 700 участников</div>
            <div class="networking__item">Компании из различных отраслей и сфер деятельности</div>
            <div class="networking__item networking__item_second">Широкий круг контактов</div>
          </div>
          <div class="networking__bottom networking__bottom_second">
            <a class="networking__btn networking__btn_second open-popup"  networking-type="business" href="#networkform-poopup">
              Оставить заявку
            </a>
            <div class="networking__bottom-info">Входит в форматы <strong>СТАНДАРТ и БИЗНЕС</strong></div>
          </div>
        </div>
      </div>
    </div>

    <!-- Блок программа семинара  -->
    <div class="schedule  forblur">
      <div class="schedule__title">Программа семинара</div>
      <div class="schedule__img">Брайан Трейси</div>
      <table class="schedule__table">
      <tr class="schedule__table-row">
        <th class="schedule__table-cell schedule__table-cell_head">Время</th>
        <th class="schedule__table-cell schedule__table-cell_head">Тема</th>
        <th class="schedule__table-cell schedule__table-cell_head schedule__table-cell_mobile">Спикер</th>
      </tr>
      <tr class="schedule__table-row">
        <td class="schedule__table-cell schedule__table-cell_time">8:30</td>
        <td class="schedule__table-cell schedule__table-cell_first">Регистрация</td>
        <td class="schedule__table-cell schedule__table-cell_mobile"></td>
      </tr>
      <tr class="schedule__table-row">
        <td class="schedule__table-cell schedule__table-cell_time">9:30</td>
        <td class="schedule__table-cell">Приветственное слово от организаторов</td>
        <td class="schedule__table-cell schedule__table-cell_mobile"></td>
      </tr>
      <tr class="schedule__table-row">
        <td class="schedule__table-cell schedule__table-cell_time">9:45</td>
        <td class="schedule__table-cell">«Как найти людей, которые <br>помогут достичь цели»<div class="schedule__mobile-speaker">Сергей Озеров</div></td>
        <td class="schedule__table-cell schedule__table-cell_second schedule__table-cell_mobile">Сергей Озеров</td>
      </tr>
      <tr class="schedule__table-row">
        <td class="schedule__table-cell schedule__table-cell_time">11:15</td>
        <td class="schedule__table-cell">Кофе-брейк</td>
        <td class="schedule__table-cell schedule__table-cell_mobile"></td>
      </tr>
      <tr class="schedule__table-row">
        <td class="schedule__table-cell schedule__table-cell_time">11:30</td>
        <td class="schedule__table-cell">«Как взрастить в себе лидера <br>и мотивировать других»<div class="schedule__mobile-speaker">Сергей Озеров</div></td>
        <td class="schedule__table-cell schedule__table-cell_second schedule__table-cell_mobile">Сергей Озеров</td>
      </tr>
      <tr class="schedule__table-row">
        <td class="schedule__table-cell schedule__table-cell_time">13:30</td>
        <td class="schedule__table-cell">Обед</td>
        <td class="schedule__table-cell schedule__table-cell_mobile"></td>
      </tr>
      <tr class="schedule__table-row">
        <td class="schedule__table-cell schedule__table-cell_time">14:00</td>
        <td class="schedule__table-cell">«Постановка целей»<br>«Преодоление внутренних барьеров на пути к успеху»<div class="schedule__mobile-speaker">Брайан Трейси</div></td>
        <td class="schedule__table-cell schedule__table-cell_second schedule__table-cell_mobile">Брайан Трейси</td>
      </tr>
      <tr class="schedule__table-row">
        <td class="schedule__table-cell schedule__table-cell_time">15:30</td>
        <td class="schedule__table-cell">Кофе-брейк</td>
        <td class="schedule__table-cell schedule__table-cell_mobile"></td>
      </tr>
      <tr class="schedule__table-row">
        <td class="schedule__table-cell schedule__table-cell_time">16:00</td>
        <td class="schedule__table-cell">«Раскрытие внутреннего потенциала».<br>«Как достигать целей быстрее»<div class="schedule__mobile-speaker">Брайан Трейси</div></td>
        <td class="schedule__table-cell schedule__table-cell_second schedule__table-cell_mobile">Брайан Трейси</td>
      </tr>
      <tr class="schedule__table-row">
        <td class="schedule__table-cell schedule__table-cell_time">17:30</td>
        <td class="schedule__table-cell">Кофе-брейк</td>
        <td class="schedule__table-cell schedule__table-cell_mobile"></td>
      </tr>
      <tr class="schedule__table-row">
        <td class="schedule__table-cell schedule__table-cell_time">17:40 - 18:20</td>
        <td class="schedule__table-cell">«Подготовка персонального плана»<div class="schedule__mobile-speaker">Брайан Трейси</div></td>
        <td class="schedule__table-cell schedule__table-cell_second schedule__table-cell_mobile">Брайан Трейси</td>
      </tr>
      <tr>
      </table>
    </div>

    <!-- Блок Форматы участия в тренинга    -->
    <div class="formsinvolv forblur">
      <div class="formsinvolv__heading">
        <h1 class="formsinvolv__mainTitle">Форматы участия в тренинге:</h1>
        <a href="#spec-formats-poopup" class="formsinvolv__corporate open-popup">Специальные условия для корпоративного участия</a>
      </div>

      <table class="formsinvolv__table">

        <tr class="formsinvolv__row formsinvolv__row_header">
          <th></th>
          <th class="formsinvolv__cell formsinvolv__cell_status formsinvolv__cell_header1">
            <div class="formsinvolv__cell-inner formsinvolv__cell-inner_standart formsinvolv__cell-inner_non-border"></div>
            <div class="formsinvolv__cell-clmtitle">Стандарт</div>
          </th>
          <th class="formsinvolv__cell formsinvolv__cell_status formsinvolv__cell_header2">
            <div class="formsinvolv__cell-inner formsinvolv__cell-inner_buisness formsinvolv__cell-inner_non-border"></div>
            <div class="formsinvolv__cell-clmtitle">Бизнес</div>
          </th>
          <th class="formsinvolv__cell formsinvolv__cell_status formsinvolv__cell_header3">
            <div class="formsinvolv__cell-inner formsinvolv__cell-inner_vip formsinvolv__cell-inner_non-border"></div>
            <div class="formsinvolv__cell-clmtitle">вип</div>
          </th>
          <th class="formsinvolv__cell formsinvolv__cell_status formsinvolv__cell_header4">
            <div class="formsinvolv__cell-inner formsinvolv__cell-inner_premium formsinvolv__cell-inner_non-border"></div>
            <div class="formsinvolv__cell-clmtitle">Платинум</div>
          </th>
        </tr>

        <tr class="formsinvolv__row">
          <td class="formsinvolv__cell formsinvolv__cell_info formsinvolv__cell_padding1">
            <div class="formsinvolv__cell-inner formsinvolv__cell-inner_white"></div>
            <div class="formsinvolv__cell-title">рабочая тетрадь</div>
            <div class="formsinvolv__cell-subtitle">которая позволит в любой момент вернуться
                                                      к данным на тренинге знаниям и мыслям, которые
                                                      будут посещать в течении обучения</div>
          </td>
          <td class="formsinvolv__cell formsinvolv__cell_status">
            <div class="formsinvolv__cell-inner formsinvolv__cell-inner_check formsinvolv__cell-inner_gray">
            </div>
          </td>
          <td class="formsinvolv__cell formsinvolv__cell_status">
            <div class="formsinvolv__cell-inner formsinvolv__cell-inner_check formsinvolv__cell-inner_white">
            </div>
          </td>
          <td class="formsinvolv__cell formsinvolv__cell_status">
            <div class="formsinvolv__cell-inner formsinvolv__cell-inner_check formsinvolv__cell-inner_gray">
            </div>
          </td>
          <td class="formsinvolv__cell formsinvolv__cell_status">
            <div class="formsinvolv__cell-inner formsinvolv__cell-inner_check formsinvolv__cell-inner_white ">
            </div>
          </td>
        </tr>

        <tr class="formsinvolv__row">
          <td class="formsinvolv__cell formsinvolv__cell_info">
            <div class="formsinvolv__cell-inner formsinvolv__cell-inner_white"></div>
            <div class="formsinvolv__cell-title">сертификат</div>
          </td>
          <td class="formsinvolv__cell formsinvolv__cell_status">
            <div class="formsinvolv__cell-inner formsinvolv__cell-inner_check formsinvolv__cell-inner_gray">
            </div>
          </td>
          <td class="formsinvolv__cell formsinvolv__cell_status">
            <div class="formsinvolv__cell-inner formsinvolv__cell-inner_check formsinvolv__cell-inner_white">
            </div>
          </td>
          <td class="formsinvolv__cell formsinvolv__cell_status">
            <div class="formsinvolv__cell-inner formsinvolv__cell-inner_check formsinvolv__cell-inner_gray">
            </div>
          </td>
          <td class="formsinvolv__cell formsinvolv__cell_status">
            <div class="formsinvolv__cell-inner formsinvolv__cell-inner_check formsinvolv__cell-inner_white">
            </div>
          </td>
        </tr>

        <tr class="formsinvolv__row">
          <td class="formsinvolv__cell formsinvolv__cell_info">
            <div class="formsinvolv__cell-inner formsinvolv__cell-inner_white"></div>
            <div class="formsinvolv__cell-title">Оборудование синхронного перевода</div>
          </td>
          <td class="formsinvolv__cell formsinvolv__cell_status">
            <div class="formsinvolv__cell-inner formsinvolv__cell-inner_check formsinvolv__cell-inner_gray">
            </div>
          </td>
          <td class="formsinvolv__cell formsinvolv__cell_status">
            <div class="formsinvolv__cell-inner formsinvolv__cell-inner_check formsinvolv__cell-inner_white">
            </div>
          </td>
          <td class="formsinvolv__cell formsinvolv__cell_status">
            <div class="formsinvolv__cell-inner formsinvolv__cell-inner_check formsinvolv__cell-inner_gray">
            </div>
          </td>
          <td class="formsinvolv__cell formsinvolv__cell_status">
            <div class="formsinvolv__cell-inner formsinvolv__cell-inner_check formsinvolv__cell-inner_white">
            </div>
          </td>
        </tr>

        <tr class="formsinvolv__row">
          <td class="formsinvolv__cell formsinvolv__cell_info">
            <div class="formsinvolv__cell-inner formsinvolv__cell-inner_white"></div>
            <div class="formsinvolv__cell-title">запись тренинга</div>
            <div class="formsinvolv__cell-subtitle">доступ по уникальной персональной ссылке
                                                  с 1 устройства в течение 30 дней</div>
          </td>
          <td class="formsinvolv__cell formsinvolv__cell_status">
            <div class="formsinvolv__cell-inner formsinvolv__cell-inner_cross formsinvolv__cell-inner_gray">
            </div>
          </td>
          <td class="formsinvolv__cell formsinvolv__cell_status">
            <div class="formsinvolv__cell-inner formsinvolv__cell-inner_cross formsinvolv__cell-inner_white">
            </div>
          </td>
          <td class="formsinvolv__cell formsinvolv__cell_status">
            <div class="formsinvolv__cell-inner formsinvolv__cell-inner_check formsinvolv__cell-inner_gray">
            </div>
          </td>
          <td class="formsinvolv__cell formsinvolv__cell_status">
            <div class="formsinvolv__cell-inner formsinvolv__cell-inner_check formsinvolv__cell-inner_white">
            </div>
          </td>
        </tr>

        <tr class="formsinvolv__row">
          <td class="formsinvolv__cell formsinvolv__cell_info">
            <div class="formsinvolv__cell-inner formsinvolv__cell-inner_white"></div>
            <div class="formsinvolv__cell-title">книга Брайана Трейси </div>
            <div class="formsinvolv__cell-subtitle">«Выйти из зоны комфорта» - книга №1 по саморазвитию.<br>*Для формата Бизнес - электронная версия</div>
          </td>
          <td class="formsinvolv__cell formsinvolv__cell_status">
            <div class="formsinvolv__cell-inner formsinvolv__cell-inner_cross formsinvolv__cell-inner_gray">
            </div>
          </td>
          <td class="formsinvolv__cell formsinvolv__cell_status">
            <div class="formsinvolv__cell-inner formsinvolv__cell-inner_check formsinvolv__cell-inner_white">
            </div>
          </td>
          <td class="formsinvolv__cell formsinvolv__cell_status">
            <div class="formsinvolv__cell-inner formsinvolv__cell-inner_check formsinvolv__cell-inner_gray">
            </div>
          </td>
          <td class="formsinvolv__cell formsinvolv__cell_status">
            <div class="formsinvolv__cell-inner formsinvolv__cell-inner_check formsinvolv__cell-inner_white">
            </div>
          </td>
        </tr>

        <tr class="formsinvolv__row">
          <td class="formsinvolv__cell formsinvolv__cell_info">
            <div class="formsinvolv__cell-inner formsinvolv__cell-inner_white"></div>
            <div class="formsinvolv__cell-title">кофе брейк</div>
          </td>
          <td class="formsinvolv__cell formsinvolv__cell_status">
            <div class="formsinvolv__cell-inner formsinvolv__cell-inner_cross formsinvolv__cell-inner_gray">
            </div>
          </td>
          <td class="formsinvolv__cell formsinvolv__cell_status">
            <div class="formsinvolv__cell-inner formsinvolv__cell-inner_check formsinvolv__cell-inner_white">
            </div>
          </td>
          <td class="formsinvolv__cell formsinvolv__cell_status">
            <div class="formsinvolv__cell-inner formsinvolv__cell-inner_check formsinvolv__cell-inner_gray">
            </div>
          </td>
          <td class="formsinvolv__cell formsinvolv__cell_status">
            <div class="formsinvolv__cell-inner formsinvolv__cell-inner_check formsinvolv__cell-inner_white">
            </div>
          </td>
        </tr>

        <tr  class="formsinvolv__row">
          <td class="formsinvolv__cell formsinvolv__cell_info">
            <div class="formsinvolv__cell-inner formsinvolv__cell-inner_white"></div>
            <div class="formsinvolv__cell-title">business-нетворкинг</div>
          </td>
          <td class="formsinvolv__cell formsinvolv__cell_status">
            <div class="formsinvolv__cell-inner formsinvolv__cell-inner_check formsinvolv__cell-inner_gray">
            </div>
          </td>
          <td class="formsinvolv__cell formsinvolv__cell_status">
            <div class="formsinvolv__cell-inner formsinvolv__cell-inner_check formsinvolv__cell-inner_white">
            </div>
          </td>
          <td class="formsinvolv__cell formsinvolv__cell_status">
            <div class="formsinvolv__cell-inner formsinvolv__cell-inner_check formsinvolv__cell-inner_gray">
            </div>
          </td>
          <td class="formsinvolv__cell formsinvolv__cell_status">
            <div class="formsinvolv__cell-inner formsinvolv__cell-inner_check formsinvolv__cell-inner_white">
            </div>
          </td>
        </tr >

        <tr  class="formsinvolv__row">
          <td class="formsinvolv__cell formsinvolv__cell_info">
            <div class="formsinvolv__cell-inner formsinvolv__cell-inner_white"></div>
            <div class="formsinvolv__cell-title">VIP-нетворкинг</div>
          </td>
          <td class="formsinvolv__cell formsinvolv__cell_status">
            <div class="formsinvolv__cell-inner formsinvolv__cell-inner_cross formsinvolv__cell-inner_gray">
            </div>
          </td>
          <td class="formsinvolv__cell formsinvolv__cell_status">
            <div class="formsinvolv__cell-inner formsinvolv__cell-inner_cross formsinvolv__cell-inner_white">
            </div>
          </td>
          <td class="formsinvolv__cell formsinvolv__cell_status">
            <div class="formsinvolv__cell-inner formsinvolv__cell-inner_check formsinvolv__cell-inner_gray">
            </div>
          </td>
          <td class="formsinvolv__cell formsinvolv__cell_status">
            <div class="formsinvolv__cell-inner formsinvolv__cell-inner_check formsinvolv__cell-inner_white">
            </div>
          </td>
        </tr >

        <tr class="formsinvolv__row">
          <td class="formsinvolv__cell formsinvolv__cell_info">
            <div class="formsinvolv__cell-inner formsinvolv__cell-inner_white"></div>
            <div class="formsinvolv__cell-title">билет на бизнес - игру "Гений переговоров"</div>
          </td>
          <td class="formsinvolv__cell formsinvolv__cell_status">
            <div class="formsinvolv__cell-inner formsinvolv__cell-inner_cross formsinvolv__cell-inner_gray">
            </div>
          </td>
          <td class="formsinvolv__cell formsinvolv__cell_status">
            <div class="formsinvolv__cell-inner formsinvolv__cell-inner_cross formsinvolv__cell-inner_white">
            </div>
          </td>
          <td class="formsinvolv__cell formsinvolv__cell_status">
            <div class="formsinvolv__cell-inner formsinvolv__cell-inner_check formsinvolv__cell-inner_gray">
            </div>
          </td>
          <td class="formsinvolv__cell formsinvolv__cell_status">
            <div class="formsinvolv__cell-inner formsinvolv__cell-inner_check formsinvolv__cell-inner_white">
            </div>
          </td>
        </tr>

        <tr class="formsinvolv__row">
          <td class="formsinvolv__cell formsinvolv__cell_info">
            <div class="formsinvolv__cell-inner formsinvolv__cell-inner_white"></div>
            <div class="formsinvolv__cell-title">лучшие места в зале</div>
          </td>
          <td class="formsinvolv__cell formsinvolv__cell_status">
            <div class="formsinvolv__cell-inner formsinvolv__cell-inner_cross formsinvolv__cell-inner_gray">
            </div>
          </td>
          <td class="formsinvolv__cell formsinvolv__cell_status">
            <div class="formsinvolv__cell-inner formsinvolv__cell-inner_cross formsinvolv__cell-inner_white">
            </div>
          </td>
          <td class="formsinvolv__cell formsinvolv__cell_status">
            <div class="formsinvolv__cell-inner formsinvolv__cell-inner_cross formsinvolv__cell-inner_gray">
            </div>
          </td>
          <td class="formsinvolv__cell formsinvolv__cell_status">
            <div class="formsinvolv__cell-inner formsinvolv__cell-inner_check formsinvolv__cell-inner_white">
            </div>
          </td>
        </tr>

        <tr class="formsinvolv__row">
          <td class="formsinvolv__cell formsinvolv__cell_info">
            <div class="formsinvolv__cell-inner formsinvolv__cell-inner_white"></div>
            <div class="formsinvolv__cell-title">аудит HR службы от компании HR Kenobi</div>
          </td>
          <td class="formsinvolv__cell formsinvolv__cell_status">
            <div class="formsinvolv__cell-inner formsinvolv__cell-inner_cross formsinvolv__cell-inner_gray">
            </div>
          </td>
          <td class="formsinvolv__cell formsinvolv__cell_status">
            <div class="formsinvolv__cell-inner formsinvolv__cell-inner_cross formsinvolv__cell-inner_white">
            </div>
          </td>
          <td class="formsinvolv__cell formsinvolv__cell_status">
            <div class="formsinvolv__cell-inner formsinvolv__cell-inner_cross formsinvolv__cell-inner_gray">
            </div>
          </td>
          <td class="formsinvolv__cell formsinvolv__cell_status">
            <div class="formsinvolv__cell-inner formsinvolv__cell-inner_check formsinvolv__cell-inner_white">
            </div>
          </td>
        </tr>

        <tr class="formsinvolv__row">
          <td class="formsinvolv__cell formsinvolv__cell_info">
            <div class="formsinvolv__cell-inner formsinvolv__cell-inner_white"></div>
            <div class="formsinvolv__cell-title">аудит отдела продаж</div>
          </td>
          <td class="formsinvolv__cell formsinvolv__cell_status">
            <div class="formsinvolv__cell-inner formsinvolv__cell-inner_cross formsinvolv__cell-inner_gray">
            </div>
          </td>
          <td class="formsinvolv__cell formsinvolv__cell_status">
            <div class="formsinvolv__cell-inner formsinvolv__cell-inner_cross formsinvolv__cell-inner_white">
            </div>
          </td>
          <td class="formsinvolv__cell formsinvolv__cell_status">
            <div class="formsinvolv__cell-inner formsinvolv__cell-inner_cross formsinvolv__cell-inner_gray">
            </div>
          </td>
          <td class="formsinvolv__cell formsinvolv__cell_status">
            <div class="formsinvolv__cell-inner formsinvolv__cell-inner_check formsinvolv__cell-inner_white">
            </div>
          </td>
        </tr>

        <tr class="formsinvolv__row">
          <td class="formsinvolv__cell formsinvolv__cell_info formsinvolv__cell_padding2">
            <div class="formsinvolv__cell-inner formsinvolv__cell-inner_white"></div>
            <div class="formsinvolv__cell-title">коуч-сессия с Александром Стародумовым</div>
            <div class="formsinvolv__cell-subtitle">бизнес-коучем Эриксоновского университета.</div>
          </td>
          <td class="formsinvolv__cell formsinvolv__cell_status">
            <div class="formsinvolv__cell-inner formsinvolv__cell-inner_cross formsinvolv__cell-inner_gray">
            </div>
          </td>
          <td class="formsinvolv__cell formsinvolv__cell_status">
            <div class="formsinvolv__cell-inner formsinvolv__cell-inner_cross formsinvolv__cell-inner_white">
            </div>
          </td>
          <td class="formsinvolv__cell formsinvolv__cell_status">
            <div class="formsinvolv__cell-inner formsinvolv__cell-inner_cross formsinvolv__cell-inner_gray">
            </div>
          </td>
          <td class="formsinvolv__cell formsinvolv__cell_status">
            <div class="formsinvolv__cell-inner formsinvolv__cell-inner_check formsinvolv__cell-inner_white">
            </div>
          </td>
        </tr>

        <tr class="formsinvolv__row">
          <td class="formsinvolv__cell formsinvolv__cell_info formsinvolv__cell_padding2">
            <div class="formsinvolv__cell-inner formsinvolv__cell-inner_white"></div>
            <div class="formsinvolv__cell-title">обед с Сергеем Озеровым</div>
          </td>
          <td class="formsinvolv__cell formsinvolv__cell_status">
            <div class="formsinvolv__cell-inner formsinvolv__cell-inner_cross formsinvolv__cell-inner_gray">
            </div>
          </td>
          <td class="formsinvolv__cell formsinvolv__cell_status">
            <div class="formsinvolv__cell-inner formsinvolv__cell-inner_cross formsinvolv__cell-inner_white">
            </div>
          </td>
          <td class="formsinvolv__cell formsinvolv__cell_status">
            <div class="formsinvolv__cell-inner formsinvolv__cell-inner_cross formsinvolv__cell-inner_gray">
            </div>
          </td>
          <td class="formsinvolv__cell formsinvolv__cell_status">
            <div class="formsinvolv__cell-inner formsinvolv__cell-inner_check formsinvolv__cell-inner_white">
            </div>
          </td>
        </tr>

        <tr class="formsinvolv__row">
          <td class="formsinvolv__cell formsinvolv__cell_info formsinvolv__cell_padding2">
            <div class="formsinvolv__cell-inner formsinvolv__cell-inner_white"></div>
            <div class="formsinvolv__cell-title">персональный менеджер</div>
          </td>
          <td class="formsinvolv__cell formsinvolv__cell_status">
            <div class="formsinvolv__cell-inner formsinvolv__cell-inner_cross formsinvolv__cell-inner_gray">
            </div>
          </td>
          <td class="formsinvolv__cell formsinvolv__cell_status">
            <div class="formsinvolv__cell-inner formsinvolv__cell-inner_cross formsinvolv__cell-inner_white">
            </div>
          </td>
          <td class="formsinvolv__cell formsinvolv__cell_status">
            <div class="formsinvolv__cell-inner formsinvolv__cell-inner_cross formsinvolv__cell-inner_gray">
            </div>
          </td>
          <td class="formsinvolv__cell formsinvolv__cell_status">
            <div class="formsinvolv__cell-inner formsinvolv__cell-inner_check formsinvolv__cell-inner_white">
            </div>
          </td>
        </tr>

        <tr class="formsinvolv__row">
          <td class="formsinvolv__cell formsinvolv__cell_info formsinvolv__cell_padding2">
            <div class="formsinvolv__cell-inner formsinvolv__cell-inner_white"></div>
            <div class="formsinvolv__cell-title">место на стоянке</div>
          </td>
          <td class="formsinvolv__cell formsinvolv__cell_status">
            <div class="formsinvolv__cell-inner formsinvolv__cell-inner_cross formsinvolv__cell-inner_gray">
            </div>
          </td>
          <td class="formsinvolv__cell formsinvolv__cell_status">
            <div class="formsinvolv__cell-inner formsinvolv__cell-inner_cross formsinvolv__cell-inner_white">
            </div>
          </td>
          <td class="formsinvolv__cell formsinvolv__cell_status">
            <div class="formsinvolv__cell-inner formsinvolv__cell-inner_cross formsinvolv__cell-inner_gray">
            </div>
          </td>
          <td class="formsinvolv__cell formsinvolv__cell_status">
            <div class="formsinvolv__cell-inner formsinvolv__cell-inner_check formsinvolv__cell-inner_white">
            </div>
          </td>
        </tr>

        <tr class="formsinvolv__row formsinvolv__row_footer">
          <th></th>
          <th class="formsinvolv__cell formsinvolv__cell_status formsinvolv__cell_footer">
            <div class="formsinvolv__cell-inner formsinvolv__cell-inner_foot formsinvolv__cell-inner_foot-standart formsinvolv__cell-inner_non-border"></div>
            <a href="#formats-poopup" format-type="standart" class="formsinvolv__cell-clmtitle formsinvolv__cell-clmtitle_foot open-popup">Узнать цену «стандарт»</a>
          </th>
          <th class="formsinvolv__cell formsinvolv__cell_status formsinvolv__cell_footer">
            <div class="formsinvolv__cell-inner formsinvolv__cell-inner_foot formsinvolv__cell-inner_foot-buisness formsinvolv__cell-inner_non-border"></div>
            <a href="#formats-poopup" format-type="business" class="formsinvolv__cell-clmtitle formsinvolv__cell-clmtitle_foot formsinvolv__cell-clmtitle_foot-buisness open-popup">Узнать цену «бизнес»</div>
          </th>
          <th class="formsinvolv__cell formsinvolv__cell_status formsinvolv__cell_footer">
            <div class="formsinvolv__cell-inner formsinvolv__cell-inner_foot formsinvolv__cell-inner_foot-vip formsinvolv__cell-inner_non-border"></div>
            <a href="#formats-poopup" format-type="vip" class="formsinvolv__cell-clmtitle formsinvolv__cell-clmtitle_foot open-popup">Узнать цену «ВИП»</div>
          </th>
          <th class="formsinvolv__cell formsinvolv__cell_status formsinvolv__cell_footer">
            <div class="formsinvolv__cell-inner formsinvolv__cell-inner_foot formsinvolv__cell-inner_foot-premium formsinvolv__cell-inner_non-border"></div>
            <a href="#formats-poopup" format-type="platinum" class="formsinvolv__cell-clmtitle formsinvolv__cell-clmtitle_foot open-popup">Узнать цену «ПЛАТИНУМ»</div>
          </th>
        </tr>

      </table>

      <div class="formsinvolv-mob">

        <div class="formsinvolv-mob__status js-ormsInvolv-mob-item">
          <div class="formsinvolv-mob__head-title formsinvolv-mob__head-title_platinum js-formsInvolv-mob-item-head">Платинум</div>
          <div class="formsinvolv-mob__hidden">
            <div class="formsinvolv-mob__line">
              <div class="formsinvolv-mob__line-title formsinvolv-mob__line-title_none-before">рабочая тетрадь</div>
              <div class="formsinvolv-mob__line-subtitle">которая позволит в любой момент
                                                        вернуться к данным на тренинге
                                                        знаниям и мыслям, которые будут
                                                        посещать в течении обучения</div>
            </div>
            <div class="formsinvolv-mob__line">
              <div class="formsinvolv-mob__line-title">сертификат</div>
            </div>
            <div class="formsinvolv-mob__line">
              <div class="formsinvolv-mob__line-title">Оборудование синхронного перевода</div>
            </div>
            <div class="formsinvolv-mob__line">
              <div class="formsinvolv-mob__line-title">запись тренинга </div>
              <div class="formsinvolv-mob__line-subtitle">доступ по уникальной персональной ссылке с 1 устройства в течение 30 дней</div>
            </div>
            <div class="formsinvolv-mob__line">
              <div class="formsinvolv-mob__line-title">книга Брайана Трейси </div>
              <div class="formsinvolv-mob__line-subtitle">«Выйти из зоны комфорта» - книга №1 по саморазвитию. </div>
            </div>
            <div class="formsinvolv-mob__line">
              <div class="formsinvolv-mob__line-title">кофе брейк</div>
            </div>
            <div class="formsinvolv-mob__line">
              <div class="formsinvolv-mob__line-title">VIP-нетворкинг</div>
            </div>
            <div class="formsinvolv-mob__line">
              <div class="formsinvolv-mob__line-title">билет на бизнес - игру «Гений переговоров»</div>
            </div>
            <div class="formsinvolv-mob__line">
              <div class="formsinvolv-mob__line-title">лучшие места в зале</div>
            </div>
            <div class="formsinvolv-mob__line">
              <div class="formsinvolv-mob__line-title">аудит HR службы от компании HR Kenobi</div>
            </div>
            <div class="formsinvolv-mob__line">
              <div class="formsinvolv-mob__line-title">аудит отдела продаж</div>
            </div>
            <div class="formsinvolv-mob__line">
              <div class="formsinvolv-mob__line-title">коуч-сессия с Александром Стародумовым</div>
              <div class="formsinvolv-mob__line-subtitle">бизнес-коучем Эриксоновского университета.</div>
            </div>

            <div class="formsinvolv-mob__line">
              <a href="#formats-poopup" format-type="platinum" class="formsinvolv-mob__line-btn formsinvolv-mob__line-btn_platinum open-popup">Узнать цену<br>«Платинум»</a>
            </div>
          </div>
        </div>


        <div class="formsinvolv-mob__status js-ormsInvolv-mob-item">
          <div class="formsinvolv-mob__head-title formsinvolv-mob__head-title_vip js-formsInvolv-mob-item-head">Вип</div>
          <div class="formsinvolv-mob__hidden">
            <div class="formsinvolv-mob__line">
              <div class="formsinvolv-mob__line-title formsinvolv-mob__line-title_none-before">рабочая тетрадь</div>
              <div class="formsinvolv-mob__line-subtitle">которая позволит в любой момент
                                                        вернуться к данным на тренинге
                                                        знаниям и мыслям, которые будут
                                                        посещать в течении обучения</div>
            </div>
            <div class="formsinvolv-mob__line">
              <div class="formsinvolv-mob__line-title">сертификат</div>
            </div>

            <div class="formsinvolv-mob__line">
              <div class="formsinvolv-mob__line-title">Оборудование синхронного перевода</div>
            </div>

            <div class="formsinvolv-mob__line">
              <div class="formsinvolv-mob__line-title">запись тренинга </div>
              <div class="formsinvolv-mob__line-subtitle">доступ по уникальной персональной ссылке с 1 устройства в течение 30 дней</div>
            </div>

            <div class="formsinvolv-mob__line">
              <div class="formsinvolv-mob__line-title">книга Брайана Трейси </div>
              <div class="formsinvolv-mob__line-subtitle">«Выйти из зоны комфорта» - книга №1 по саморазвитию. </div>
            </div>

            <div class="formsinvolv-mob__line">
              <div class="formsinvolv-mob__line-title">кофе брейк</div>
            </div>

            <div class="formsinvolv-mob__line">
              <div class="formsinvolv-mob__line-title">VIP-нетворкинг</div>
            </div>

            <div class="formsinvolv-mob__line">
              <div class="formsinvolv-mob__line-title">билет на бизнес - игру «Гений переговоров»</div>
            </div>

            <div class="formsinvolv-mob__line">
              <a href="#formats-poopup" format-type="vip" class="formsinvolv-mob__line-btn formsinvolv-mob__line-btn_vip open-popup">Узнать цену<br>«ВИП»</a>
            </div>
          </div>
        </div>

        <div class="formsinvolv-mob__status js-ormsInvolv-mob-item">
          <div class="formsinvolv-mob__head-title formsinvolv-mob__head-title_buisness js-formsInvolv-mob-item-head">Бизнес</div>
          <div class="formsinvolv-mob__hidden">
            <div class="formsinvolv-mob__line">
              <div class="formsinvolv-mob__line-title formsinvolv-mob__line-title_none-before">рабочая тетрадь</div>
              <div class="formsinvolv-mob__line-subtitle">которая позволит в любой момент
                                                        вернуться к данным на тренинге
                                                        знаниям и мыслям, которые будут
                                                        посещать в течении обучения</div>
            </div>
            <div class="formsinvolv-mob__line">
              <div class="formsinvolv-mob__line-title">сертификат</div>
            </div>

            <div class="formsinvolv-mob__line">
              <div class="formsinvolv-mob__line-title">Оборудование синхронного перевода</div>
            </div>

            <div class="formsinvolv-mob__line">
              <div class="formsinvolv-mob__line-title">книга Брайана Трейси </div>
              <div class="formsinvolv-mob__line-subtitle">«Выйти из зоны комфорта» - книга №1 по саморазвитию.<br>*Для формата Бизнес - электронная версия</div>
            </div>

            <div class="formsinvolv-mob__line">
              <div class="formsinvolv-mob__line-title">кофе брейк</div>
            </div>

            <div class="formsinvolv-mob__line">
              <a href="#formats-poopup" format-type="business"  class="formsinvolv-mob__line-btn formsinvolv-mob__line-btn_business open-popup" >Узнать цену<br>«БИЗНЕС»</a>
            </div>
          </div>
        </div>

        <div class="formsinvolv-mob__status js-ormsInvolv-mob-item">
          <div class="formsinvolv-mob__head-title formsinvolv-mob__head-title_standart js-formsInvolv-mob-item-head">Стандарт</div>
          <div class="formsinvolv-mob__hidden">
            <div class="formsinvolv-mob__line">
              <div class="formsinvolv-mob__line-title formsinvolv-mob__line-title_none-before">рабочая тетрадь</div>
              <div class="formsinvolv-mob__line-subtitle">которая позволит в любой момент
                                                        вернуться к данным на тренинге
                                                        знаниям и мыслям, которые будут
                                                        посещать в течении обучения</div>
            </div>
            <div class="formsinvolv-mob__line">
              <div class="formsinvolv-mob__line-title">сертификат</div>
            </div>

            <div class="formsinvolv-mob__line">
              <div class="formsinvolv-mob__line-title">Оборудование синхронного перевода</div>
            </div>

            <div class="formsinvolv-mob__line">
              <a href="#formats-poopup" format-type="standart" class="formsinvolv-mob__line-btn formsinvolv-mob__line-btn_standart open-popup">Узнать цену<br>«СТАНДАРТ»</a>
            </div>
          </div>
        </div>

      </div>
    </div>

    <!-- ссылка на онлайн трансляцию  -->
    <div class="switch-link forblur">
      <div class="switch-link__continer">
        <div class="switch-link__info switch-link__info_online">
          <div class="switch-link__info-img switch-link__info-img_online"></div>
          <div class="switch-link__info-text">
            <div class="switch-link__title switch-link__title_online">ОНЛАЙН-трансляция</div>
            <div class="switch-link__subtitle switch-link__subtitle_online">по всему миру</div>
          </div>
        </div>
        <a href="http://tracyural.online" class="switch-link__button">Получить доступ</a>
      </div>
    </div>

    <!-- Блок Радислав ГАНДАПАС -->
    <div class="gandapas forblur">
      <div class="gandapas__container">
        <img class="gandapas__img-person" src="images/gandapas/right.png">
        <div class="gandapas__opinion">
          <div class="gandapas__opinion-name">Радислав Гандапас </div>
          <div class="gandapas__opinion-profession">самый титулованный бизнес-тренер России</div>
          <div class="gandapas__opinion-text">"Брайан Трейси - не просто человек-легенда, или Гуру бизнеса.
                                                Он сам иронически относится к подобным определениям.
                                                В первую очередь, это человек, создавший, развивший сотни компаний
                                                по всему миру. Его опыт бесценен, знания глубоки и манера их передачи -
                                                легка и очень эффективна."
          </div>
        </div>
        <div class="gandapas__img-quote"></div>
      </div>
    </div>



    <!-- Блок Видео отзыв и выступление Трейси -->
    <div class="video-otziv forblur" id="video-otziv">
      <div class="container container__wide video-otziv__conteiner">
        <div class="video-otziv__title">ПОСМОТРИТЕ ФРАГМЕНТ ВЫСТУПЛЕНИЯ И ВИДЕО-ОТЗЫВЫ</div>
        <div class="player video-otziv__video">
          <div class="video-otziv__video-poster js-video-otziv-play">
            <img src="/images/video-otziv/poster.jpg" class="video-otziv__video-poster-img">
            <div class="video-otziv__video-poster-toner"></div>
            <div class="video-otziv__video-poster-play"></div>
          </div>
          <div data-type="youtube" data-video-id="AqbNgOpVG7o"></div>
        </div>
        <div class="video-otziv__btn-wr">
          <a href="#video-otziv-poopup" class="btn btn-yellow btn-yellow-on-white video-otziv__btn open-popup">Занять место в зале</a>
        </div>
      </div>
    </div>

    <!-- Блок Мероприятия, которые мы организовали -->
    <div class="measures-ms forblur">
      <div class="container container__wide">
        <div class="measures-ms__heading">
          <h1 class="measures-ms__title">Мероприятия, которые мы организовали</h1>
          <h2 class="measures-ms__subtitle">Компания Маликспейс, организует крутые тренинги, массовый нетворкинг,
                                        семинары ведущих спикеров федерального и мирового уровня</h2>
        </div>

        <div class="measures-ms__events">
          <div class="measures-ms__events-arrow measures-ms__events-arrow_prev js-measures-ms-prev"></div>
          <div class="measures-ms__events-arrow measures-ms__events-arrow_next js-measures-ms-next"></div>
          <div class="measures-ms__number measures-ms__number_top">
            <div class="measures-ms__number-cur js-measure-current-num"></div>
            <div class="measures-ms__number-total js-measure-total-num"></div>
          </div>

          <div class="measures-ms__items owl-carousel owl-theme">
            <div class="measures-ms__item">
              <div class="measures-ms__item-foto">
                <img class="measures-ms__item-foto-img" src="images/measures-ms/networking.jpg">
              </div>
              <div class="measures-ms__item-info">
                <div class="measures-ms__number measures-ms__number_in-item">
                  <div class="measures-ms__number-cur js-measure-current-num"></div>
                  <div class="measures-ms__number-total js-measure-total-num"></div>
                </div>
                <div class="measures-ms__text">
                  <div class="measures-ms__text-name">Самый массовый нетворкинг в Челябинске</div>
                  <div class="measures-ms__text-about">Особенность события: первый массовый нетворкинг в городе</div>
                </div>
                <div class="measures-ms__infonumbers">
                  <div class="measures-ms__calendar">
                    <img class="measures-ms__calendar-img" src="images/measures-ms/calendar.png">
                    <div class="measures-ms__calendar-date">14 июня 2016</div>
                  </div>
                  <div class="measures-ms__visiting">
                    <img class="measures-ms__visiting-img" src="images/measures-ms/team.png">
                    <div class="measures-ms__visiting-number">650 человек</div>
                  </div>
                </div>
              </div>
            </div>
            <div class="measures-ms__item">
              <div class="measures-ms__item-foto">
                <img class="measures-ms__item-foto-img" src="images/measures-ms/Mann2.jpg">
              </div>
              <div class="measures-ms__item-info">
                <div class="measures-ms__number measures-ms__number_in-item">
                  <div class="measures-ms__number-cur js-measure-current-num"></div>
                  <div class="measures-ms__number-total js-measure-total-num"></div>
                </div>
                <div class="measures-ms__text">
                  <div class="measures-ms__text-name">Семинар Игоря Манна "Всего 15 дней"</div>
                  <div class="measures-ms__text-about">Особенность события: Игорь Манн, маркетолог №1 в России, простоял в планке 7 минут вместе с участниками</div>
                </div>
                <div class="measures-ms__infonumbers">
                  <div class="measures-ms__calendar">
                    <img class="measures-ms__calendar-img" src="images/measures-ms/calendar.png">
                    <div class="measures-ms__calendar-date">8 октября 2013</div>
                  </div>
                  <div class="measures-ms__visiting">
                    <img class="measures-ms__visiting-img" src="images/measures-ms/team.png">
                    <div class="measures-ms__visiting-number">350 человек</div>
                  </div>
                </div>
              </div>
            </div>
            <div class="measures-ms__item">
              <div class="measures-ms__item-foto">
                <img class="measures-ms__item-foto-img" src="images/measures-ms/Voronin.jpg">
              </div>
              <div class="measures-ms__item-info">
                <div class="measures-ms__number measures-ms__number_in-item">
                  <div class="measures-ms__number-cur js-measure-current-num"></div>
                  <div class="measures-ms__number-total js-measure-total-num"></div>
                </div>
                <div class="measures-ms__text">
                  <div class="measures-ms__text-name">Бизнес-конференция "Точные действия на миллион"</div>
                  <div class="measures-ms__text-about">Особенность события: выступление самого дорогого спикера страны Алексея Воронина</div>
                </div>
                <div class="measures-ms__infonumbers">
                  <div class="measures-ms__calendar">
                    <img class="measures-ms__calendar-img" src="images/measures-ms/calendar.png">
                    <div class="measures-ms__calendar-date">28 января 2017</div>
                  </div>
                  <div class="measures-ms__visiting">
                    <img class="measures-ms__visiting-img" src="images/measures-ms/team.png">
                    <div class="measures-ms__visiting-number">700 человек</div>
                  </div>
                </div>
              </div>
            </div>
            <div class="measures-ms__item">
              <div class="measures-ms__item-foto">
                <img class="measures-ms__item-foto-img" src="images/measures-ms/Azimov.jpg">
              </div>
              <div class="measures-ms__item-info">
                <div class="measures-ms__number measures-ms__number_in-item">
                  <div class="measures-ms__number-cur js-measure-current-num"></div>
                  <div class="measures-ms__number-total js-measure-total-num"></div>
                </div>
                <div class="measures-ms__text">
                  <div class="measures-ms__text-name">Курс Сергея Азимова "Продажи и переговоры"</div>
                  <div class="measures-ms__text-about">Курс прошел настолько масштабно, что гости из Екатеринбурга просили повторить его в их городе и команда Malikspace организовала это же мероприятие в Екатеринбурге через 1,5 месяца</div>
                </div>
                <div class="measures-ms__infonumbers">
                  <div class="measures-ms__calendar">
                    <img class="measures-ms__calendar-img" src="images/measures-ms/calendar.png">
                    <div class="measures-ms__calendar-date">7-8 апреля 2017</div>
                  </div>
                  <div class="measures-ms__visiting">
                    <img class="measures-ms__visiting-img" src="images/measures-ms/team.png">
                    <div class="measures-ms__visiting-number">550 человек</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="photos-slider photos-slider_measures">
        <div class="photos-slider__items owl-carousel owl-theme">
          <div class="photos-slider__item">
            <a href="images/ms-gallery/680/1.jpg" data-lightbox="tracy-gallery" >
              <img src="images/ms-gallery/330/1.jpg" class="photos-slider__item-img">
            </a>
          </div>
          <div class="photos-slider__item">
            <a href="images/ms-gallery/680/2.jpg" data-lightbox="tracy-gallery" >
              <img src="images/ms-gallery/330/2.jpg" class="photos-slider__item-img">
            </a>
          </div>
          <div class="photos-slider__item">
            <a href="images/ms-gallery/680/3.jpg" data-lightbox="tracy-gallery" >
              <img src="images/ms-gallery/330/3.jpg" class="photos-slider__item-img">
            </a>
          </div>
          <div class="photos-slider__item">
            <a href="images/ms-gallery/680/5.jpg" data-lightbox="tracy-gallery" >
              <img src="images/ms-gallery/330/5.jpg" class="photos-slider__item-img">
            </a>
          </div>
          <div class="photos-slider__item">
            <a href="images/ms-gallery/680/6.jpg" data-lightbox="tracy-gallery" >
              <img src="images/ms-gallery/330/6.jpg" class="photos-slider__item-img">
            </a>
          </div>
          <div class="photos-slider__item">
            <a href="images/ms-gallery/680/7.jpg" data-lightbox="tracy-gallery" >
              <img src="images/ms-gallery/330/7.jpg" class="photos-slider__item-img">
            </a>
          </div>
          <div class="photos-slider__item">
            <a href="images/ms-gallery/680/8.jpg" data-lightbox="tracy-gallery" >
              <img src="images/ms-gallery/330/8.jpg" class="photos-slider__item-img">
            </a>
          </div>
          <div class="photos-slider__item">
            <a href="images/ms-gallery/680/9.jpg" data-lightbox="tracy-gallery" >
              <img src="images/ms-gallery/330/9.jpg" class="photos-slider__item-img">
            </a>
          </div>
          <div class="photos-slider__item">
            <a href="images/ms-gallery/680/10.jpg" data-lightbox="tracy-gallery" >
              <img src="images/ms-gallery/330/10.jpg" class="photos-slider__item-img">
            </a>
          </div>
          <div class="photos-slider__item">
            <a href="images/ms-gallery/680/11.jpg" data-lightbox="tracy-gallery" >
              <img src="images/ms-gallery/330/11.jpg" class="photos-slider__item-img">
            </a>
          </div>
          <div class="photos-slider__item">
            <a href="images/ms-gallery/680/12.jpg" data-lightbox="tracy-gallery" >
              <img src="images/ms-gallery/330/12.jpg" class="photos-slider__item-img">
            </a>
          </div>
        </div>
      </div>
    </div>

    <!--Наши партнёры -->

    <div class="section partners" id="partners">
      <div class="container container__wide">
        <div class="bordered-box">
          <div class="bordered-box-title"><span>Наши партеры</span></div>
          <div class="bordered-box-content">
            <div class="logos-items logos-items_patners">
              <div class="logos-item logos-item_five">
                <img src="/images/partners/metro.jpg">
              </div>
              <div class="logos-item logos-item_five">
                <img src="/images/partners/geometria.png">
              </div>
              <div class="logos-item logos-item_five">
                <img src="/images/partners/sparta.jpg">
              </div>
              <div class="logos-item logos-item_five">
                <img src="/images/partners/2gis.png">
              </div>
              <div class="logos-item logos-item_five">
                <img src="/images/partners/h2o.jpg">
              </div>
              <div class="logos-item logos-item_five">
                <img src="/images/partners/prodvijenie.jpg">
              </div>
              <div class="logos-item logos-item_five">
                <img src="/images/partners/alpina.jpg">
              </div>
              <div class="logos-item logos-item_five">
                <img src="/images/partners/etalon.png">
              </div>
              <div class="logos-item logos-item_five">
                <img src="/images/partners/vsetreningi.png">
              </div>
            </div>
          </div>
          <a href="#partners-poopup" class="btn btn-yellow btn-yellow-on-white partners__btn open-popup">Стать партнёром</a>
        </div>
      </div>
    </div>

    <!-- Блок с картой -->
    <div class="location forblur">
      <div class="location__adress">
        <div class="location__adress-info">
          <div class="location__date">27 октября</div>
          <div class="location__place">Конгресс Холл</div>
          <div class="location__item">г. Челябинск, пр. Ленина, 35</div>
        </div>
      </div>
      <div class="location__map js-map-wrap">
        <!--  -->
      </div>
    </div>

    <!-- Блок футер  -->
    <footer id="contacts" class="section">
      <div class="footer-top">
        <div class="container container__wide">
          <div class="footer-top__adress">
            <div class="footer-top__adress-date">27 октября</div>
            <div class="footer-top__adress-place">Конгресс Холл</div>
            <div class="footer-top__adress-item">г. Челябинск, пр. Ленина, 35</div>
          </div>
          <div class="footer-top__form">
            <div class="damn-border damn-border_footer"><span class="db-top"></span><span class="db-left"></span><span class="db-right"></span><span class="db-bottom"></span></div>
            <div class="foot-title">Билеты в продаже!</div>
            <div class="foot-text"><strong>Зарегистрируйся сейчас</strong> и выбери лучшие места по комфортной цене!</div>
            <form class="custom-form foot-form">
              <div class="form-inputs">
                <input type="text" name="email"  autocomplete="off" placeholder="Электронная почта"class="required" required="required">
                <input type="text" name="phone"  autocomplete="off" placeholder="Ваш телефон" class="required" required="required">
                <input type="hidden" name="form" value="Форма в футере">
                <input type="hidden" name="type" value="full">
                <input type="hidden" name="step" value="first">
                <input type="hidden" name="crm-prject" value="1">
              </div>
              <div class="form-btn">
                <div class="btn btn-yellow btn-send">Зарегистрироваться</div>
              </div>
            </form>
          </div>
        </div>
        <div class="container container__wide">
          <div class="foot-share">
            <div class="foot-share-title">
              рекомендовать<br>мероприятие:
            </div>
            <div class="foot-share-icons">


              <a href=""  onclick=""  class="social_share js-social-share-fb" data-type="fb" title="Добавить в Facebook"><img src="/images/fb.png" alt="Добавить в Facebook"></a>
              <a href="http://vk.com/share.php?url=http://tracyural.ru"  onclick="window.open('http://vk.com/share.php?url=http://tracyural.ru', 'Рекомендовать вконтакте', 'width=300,height=250'); return false;" class="social_share" data-type="vk" title="Добавить в ВКонтакте"><img src="/images/vk.png" alt="Добавить в ВКонтакте"></a>

            </div>
          </div>
        </div>
      </div>
      <div class="footer-bottom">
        <div class="container">
          <div class="footer-logo"><img src="/images/footer-logo.png"></div>
          <a class="footer-politics open-popup" href="#politics">политика конфиденциальности</a>
          <div class="footer-phone"><a href="tel:8-800-222-35-42">8-800-222-35-42</a> звонок бесплатный</div>
        </div>
      </div>
    </footer>

    <div id="additional" class="mfp-hide">
      <div class="popup-content">
        <div class="close"></div>
        <div class="popup-title">
          СПАСИБО!
        </div>
        <div class="side-popup-pre">Пройдите регистрацию, заполнив поля «Имя» и «E-mail»</div>
        <form class="side-popup-form">
          <div class="form-inputs">
            <input type="text" name="name"  autocomplete="off" placeholder="Ваше имя" data-err="Введите имя!" class="required" required="required"/>
            <input style="display: none;" type="text" name="phone"  autocomplete="off"/>
            <input type="text" name="email"  autocomplete="off" placeholder="Ваш e-mail" data-err="Введите e-mail!" class="required" required="required"/>
          </div>

          <input type="hidden" name="form">
          <input type="hidden" name="step" value="second">
          <input type="hidden" name="crm-prject" value="1">
          <input type="hidden" name="id">
          <div class="center-button">
            <div class="btn btn-send btn-cyann">Отправить</div>
          </div>
        </form>

      </div>
    </div>

    <div id="side-popup">
      <div class="side-popup-toggle"><span>Получить билет</span></div>
      <div class="side-popup-content">
        <div class="close"></div>
        <div class="side-popup-content-inner">
          <div class="side-popup-pre">Пройдите регистрацию, заполнив поля «Имя» / «E-mail» / «Телефон»</div>
          <form class="side-popup-form">
            <div class="form-inputs">
              <input type="text" name="name" autocomplete="off" placeholder="Ваше имя" data-err="Введите имя!" class="required" required="required">
              <input type="text" name="phone" autocomplete="off" placeholder="Номер телефона" data-err="Введите номер!" class="required" required="required">
              <input type="text" name="email" autocomplete="off" placeholder="Ваш e-mail" data-err="Введите e-mail!" class="required" required="required">
            </div>

            <input type="hidden" name="form" value="Боковая кнопка - получить билет">
            <input type="hidden" name="question" value="">
            <input type="hidden" name="step" value="first">
            <input type="hidden" name="type" value="full">
            <input type="hidden" name="crm-prject" value="1">
            <input type="hidden" name="id">
            <div class="center-button">
              <div class="btn btn-send btn-yellow">Получить билет</div>
            </div>
          </form>
          <div class="side-popup-after">* Обратите внимание, что количество мест ограничено!</div>
        </div>
      </div>
    </div>

    <div id="intro-poopup" class="popup-form">
      <div class="popup-form__title popup-form__title_intro"><b>Брайн Трейси</b> в Челябинске</div>
      <div class="popup-form__info popup-form__info_intro">Пройдите регистрацию, заполнив поля «Имя» / «E-mail» / «Телефон»</div>
      <form class="popup-form__form">
        <div class="form-inputs">
          <input type="text" name="name" autocomplete="off" placeholder="Ваше имя" data-err="Введите имя!" class="popup-form__form-inp required" required="required">
          <input type="text" name="phone" autocomplete="off" placeholder="Номер телефона" data-err="Введите номер!" class="popup-form__form-inp required" required="required">
          <input type="text" name="email" autocomplete="off" placeholder="Ваш e-mail" data-err="Введите e-mail!" class="popup-form__form-inp required" required="required">
        </div>

        <input type="hidden" name="form" value="Форма в первом блоке">
        <input type="hidden" name="question" value="">
        <input type="hidden" name="step" value="first">
        <input type="hidden" name="type" value="full">
        <input type="hidden" name="crm-prject" value="1">
        <input type="hidden" name="id">
        <div class="center-button">
          <div class="btn btn-send btn-yellow popup-form__form-send-btn">Получить билет</div>
        </div>
      </form>
    </div>

    <div id="for-who-mobile-popup" class="popup-form">
      <div class="popup-form__title popup-form__title_for-who-mobile"><b>Брайн Трейси</b> в Челябинске</div>
      <div class="popup-form__info popup-form__info_for-who-mobile">Пройдите регистрацию, заполнив поля «Имя» / «E-mail» / «Телефон»</div>
      <form class="popup-form__form">
        <div class="form-inputs">
          <input type="text" name="name" autocomplete="off" placeholder="Ваше имя" data-err="Введите имя!" class="popup-form__form-inp required" required="required">
          <input type="text" name="phone" autocomplete="off" placeholder="Номер телефона" data-err="Введите номер!" class="popup-form__form-inp required" required="required">
          <input type="text" name="email" autocomplete="off" placeholder="Ваш e-mail" data-err="Введите e-mail!" class="popup-form__form-inp required" required="required">
        </div>

        <input type="hidden" name="form" class="js-for-who-mobile-popup-formname" value="">
        <input type="hidden" name="question" value="">
        <input type="hidden" name="step" value="first">
        <input type="hidden" name="type" value="full">
        <input type="hidden" name="crm-prject" value="1">
        <input type="hidden" name="id">
        <div class="center-button">
          <div class="btn btn-send btn-yellow popup-form__form-send-btn">Получить билет</div>
        </div>
      </form>
    </div>

    <div id="networkform-poopup" class="popup-form">
      <!-- <div class="popup-form__title popup-form__title_network"><strong>NETWORKING</strong> регистрация</div>       -->
      <div class="popup-form__info popup-form__info_network">Пройдите регистрацию, заполнив поля «Имя» / «E-mail» / «Телефон»</div>
      <form class="popup-form__form">
        <div class="form-inputs">
          <input type="text" name="name" autocomplete="off" placeholder="Ваше имя" data-err="Введите имя!" class="popup-form__form-inp required" required="required">
          <input type="text" name="phone" autocomplete="off" placeholder="Номер телефона" data-err="Введите номер!" class="popup-form__form-inp required" required="required">
          <input type="text" name="email" autocomplete="off" placeholder="Ваш e-mail" data-err="Введите e-mail!" class="popup-form__form-inp required" required="required">
        </div>

        <input type="hidden" name="form" class="js-networking-popup-formname" value="">
        <input type="hidden" name="question" value="">
        <input type="hidden" name="step" value="first">
        <input type="hidden" name="type" value="full">
        <input type="hidden" name="crm-prject" value="1">
        <input type="hidden" name="id">
        <div class="center-button">
          <div class="btn btn-send btn-yellow popup-form__form-send-btn">Получить билет</div>
        </div>
      </form>
    </div>

    <div id="formats-poopup" class="popup-form">
      <div class="popup-form__title popup-form__title_formats">УЗНАТЬ ЦЕНУ <span class="js-formats-poopup-type"><b>«СТАНДАРТ»</b</span>></div>
      <div class="popup-form__info popup-form__info_formats">Заполнив поля «Имя» / «E-mail» / «Телефон»</div>
      <form class="popup-form__form">
        <div class="form-inputs">
          <input type="text" name="name" autocomplete="off" placeholder="Ваше имя" data-err="Введите имя!" class="popup-form__form-inp required" required="required">
          <input type="text" name="phone" autocomplete="off" placeholder="Номер телефона" data-err="Введите номер!" class="popup-form__form-inp required" required="required">
          <input type="text" name="email" autocomplete="off" placeholder="Ваш e-mail" data-err="Введите e-mail!" class="popup-form__form-inp required" required="required">
        </div>

        <input type="hidden" name="form" class="js-formats-poopup-form-name" value="">
        <input type="hidden" name="question" value="">
        <input type="hidden" name="step" value="first">
        <input type="hidden" name="type" value="full">
        <input type="hidden" name="crm-prject" value="1">
        <input type="hidden" name="id">
        <div class="center-button">
          <div class="btn btn-send btn-yellow popup-form__form-send-btn">Узнать цену</div>
        </div>
      </form>
    </div>

    <div id="spec-formats-poopup" class="popup-form">
      <div class="popup-form__title popup-form__title_spec-formats">Узнать условия для <b>корпоративного участия</b></div>
      <div class="popup-form__info popup-form__info_formats">Заполнив поля «Имя» / «E-mail» / «Телефон»</div>
      <form class="popup-form__form">
        <div class="form-inputs">
          <input type="text" name="name" autocomplete="off" placeholder="Ваше имя" data-err="Введите имя!" class="popup-form__form-inp required" required="required">
          <input type="text" name="phone" autocomplete="off" placeholder="Номер телефона" data-err="Введите номер!" class="popup-form__form-inp required" required="required">
          <input type="text" name="email" autocomplete="off" placeholder="Ваш e-mail" data-err="Введите e-mail!" class="popup-form__form-inp required" required="required">
        </div>

        <input type="hidden" name="form" class="js-formats-poopup-form-name" value="Узнать условия для корпоративного участия">
        <input type="hidden" name="question" value="">
        <input type="hidden" name="step" value="first">
        <input type="hidden" name="type" value="full">
        <input type="hidden" name="crm-prject" value="1">
        <input type="hidden" name="id">
        <div class="center-button">
          <div class="btn btn-send btn-yellow popup-form__form-send-btn">Узнать условия</div>
        </div>
      </form>
    </div>

    <div id="video-otziv-poopup" class="popup-form">
    <div class="popup-form__title popup-form__title_video-otziv"><b>Брайн Трейси</b> в Челябинске</div>
      <div class="popup-form__info popup-form__info_video-otziv">Пройдите регистрацию, заполнив поля «Имя» / «E-mail» / «Телефон»</div>
      <form class="popup-form__form">
        <div class="form-inputs">
          <input type="text" name="name" autocomplete="off" placeholder="Ваше имя" data-err="Введите имя!" class="popup-form__form-inp required" required="required">
          <input type="text" name="phone" autocomplete="off" placeholder="Номер телефона" data-err="Введите номер!" class="popup-form__form-inp required" required="required">
          <input type="text" name="email" autocomplete="off" placeholder="Ваш e-mail" data-err="Введите e-mail!" class="popup-form__form-inp required" required="required">
        </div>

        <input type="hidden" name="form" value="Фрагмент выступления и видео-отзыв">
        <input type="hidden" name="question" value="">
        <input type="hidden" name="step" value="first">
        <input type="hidden" name="type" value="full">
        <input type="hidden" name="crm-prject" value="1">
        <input type="hidden" name="id">
        <div class="center-button">
          <div class="btn btn-send btn-yellow popup-form__form-send-btn">Получить билет</div>
        </div>
      </form>
    </div>

    <div id="partners-poopup" class="popup-form">
      <div class="popup-form__title popup-form__title_partners">Стать партнёром</div>
      <div class="popup-form__info popup-form__info_partners">Заполнив поля «Имя» / «E-mail» / «Телефон»</div>
      <form class="popup-form__form">
        <div class="form-inputs">
          <input type="text" name="name" autocomplete="off" placeholder="Ваше имя" data-err="Введите имя!" class="popup-form__form-inp required" required="required">
          <input type="text" name="phone" autocomplete="off" placeholder="Номер телефона" data-err="Введите номер!" class="popup-form__form-inp required" required="required">
          <input type="text" name="email" autocomplete="off" placeholder="Ваш e-mail" data-err="Введите e-mail!" class="popup-form__form-inp required" required="required">
        </div>

        <input type="hidden" name="form"  value="Стать партнёром">
        <input type="hidden" name="question" value="">
        <input type="hidden" name="step" value="first">
        <input type="hidden" name="type" value="full">
        <input type="hidden" name="crm-prject" value="1">
        <input type="hidden" name="id">
        <div class="center-button">
          <div class="btn btn-send btn-yellow popup-form__form-send-btn">Отправить</div>
        </div>
      </form>
    </div>


    <div id="online-transl-popup" class="popup-form">
      <div class="popup-form__info popup-form__info_online-transl">Пройдите регистрацию, заполнив поля «Имя» / «E-mail» / «Телефон»</div>
      <form class="popup-form__form">
        <div class="form-inputs">
          <input type="text" name="name" autocomplete="off" placeholder="Ваше имя" data-err="Введите имя!" class="popup-form__form-inp required" required="required">
          <input type="text" name="phone" autocomplete="off" placeholder="Номер телефона" data-err="Введите номер!" class="popup-form__form-inp required" required="required">
          <input type="text" name="email" autocomplete="off" placeholder="Ваш e-mail" data-err="Введите e-mail!" class="popup-form__form-inp required" required="required">
        </div>

        <input type="hidden" name="form" value="Регистрация на онлайн трансляцию">
        <input type="hidden" name="question" value="">
        <input type="hidden" name="step" value="first">
        <input type="hidden" name="type" value="full">
        <input type="hidden" name="crm-prject" value="1">
        <input type="hidden" name="id">
        <div class="center-button">
          <div class="btn btn-send btn-yellow popup-form__form-send-btn">Получить доступ</div>
        </div>
      </form>
    </div>

    <div id="message-sent" class="mfp-hide tiny-popup">
      <div class="popup-content">
        <div class="close"></div>
        <div class="popup-title">Спасибо!</div>
        <div class="popup-text">Ваша заявка принята, мы свяжемся с Вами в ближайшее время.</div>
      </div>
    </div>

    <div id="message-sent" class="mfp-hide tiny-popup">
      <div class="popup-content">
        <div class="close"></div>
        <div class="popup-title">Спасибо!</div>
        <div class="popup-text">Ваша заявка принята, мы свяжемся с Вами в ближайшее время.</div>
      </div>
    </div>



    <div id="politics" class="mfp-hide">
      <div class="popup-content">
        <div class="close"></div>
        <div class="popup-title">Политика конфиденциальности</div>
        <div class="popup-text">
          <p>
             Политика конфиденциальности ИП Гафарова А.А.
          </p>
          <p>
             &nbsp;
          </p>
          <p>
             1.&nbsp;&nbsp;&nbsp;&nbsp; Общие положения
          </p>
          <p>
             Настоящее положение о политике конфиденциальности (далее – «Положение») является официальным документом ИП Гафарова А.А., расположенного по адресу: Российская Федерация, Республика Башкортостан, город Уфа, ул. Карла Маркса 60, кв. 42 (далее – «Компания» / «Оператор»), и определяет порядок обработки и защиты информации о физических лицах (далее – «Пользователи»), пользующихся сервисами, информацией, услугами, программами и продуктами сайта, расположенного на доменном имени <a href="http://www.malikspace.com">www.malikspace.com</a>
            (далее – «Сайт»).
          </p>
          <p>
             &nbsp;
          </p>
          <p>
             Соблюдение конфиденциальности важно для Компании, так как целью данной Политики конфиденциальности является обеспечение защиты прав и свобод человека и гражданина при обработке его персональных данных, в том числе защиты прав на неприкосновенность частной жизни, личную и семейную тайну
          </p>
          <p>
             &nbsp;
          </p>
          <p>
             Соблюдение конфиденциальности важно для Компании, так как целью данной Политики конфиденциальности является обеспечение защиты прав и свобод человека и гражданина при обработке его персональных данных, в том числе защиты прав на неприкосновенность частной жизни, личную и семейную тайну, от несанкционированного доступа и разглашения.
          </p>
          <p>
             &nbsp;
          </p>
          <p>
             Любые действия (операции) или совокупность действий (операций), совершаемых с использованием средств автоматизации или без использования таких средств с персональными данными, включая сбор, запись, систематизацию, накопление, хранение, уточнение (обновление, изменение), извлечение и использование, передачу (распространение, предоставление, доступ), обезличивание, блокирование, удаление, уничтожение персональных данных — это действия, которые осуществляются при обработке персональных данных.
          </p>
          <p>
             &nbsp;
          </p>
          <p>
             Отношения, связанные с обработкой персональных данных регламентируются настоящим Положением, иными официальными документами Компании и действующим законодательством Российской Федерации.
          </p>
          <p>
             &nbsp;
          </p>
          <p>
             Обработка персональных данных осуществляется Компанией на законной и справедливой основе, Компания действует на на разумных и добросовестных принципах, таких как: законные цели и законные способы обработки персональных данных; добросовестность, соответствие объема и характера обрабатываемых персональных данных; соответствие целей обработки персональных данных целям, заранее определенным и заявленным при сборе персональных данных, заранее определенным и заявленным при сборе персональных данных, а также полномочиям Компании.
          </p>
          <p>
             &nbsp;
          </p>
          <p>
             Настоящее Положение регулирует любой вид обработки персональных данных и информации личного характера о физических лицах, которые являются потребителями услуг Компании.
          </p>
          <p>
             &nbsp;
          </p>
          <p>
             Настоящие Положения о политике конфиденциальности распространяется на обработку личных, персональных данных, собранных любыми средствами, как активными, так и пассивными, как через Интернет, так и без его использования.
          </p>
          <p>
             &nbsp;
          </p>
          <p>
             2. Сбор персональных данных
          </p>
          <p>
             &nbsp;
          </p>
          <p>
             Целью обработки персональных данных является выполнения обязательств Компании/Оператора перед пользователями в отношении использования Сайта и его сервисов.
          </p>
          <p>
             &nbsp;
          </p>
          <p>
             Обработка персональных данных пользователей осуществляется с согласия субъекта персональных данных на обработку его персональных данных. Под персональными данными понимается любая информация, относящаяся к прямо или косвенно определенному или определяемому физическому лицу (субъекту персональных данных) и которая может быть использована для идентификации определенного лица либо без связи с ним.
          </p>
          <p>
             &nbsp;
          </p>
          <p>
             Компания может запросить персональные данные у лица в любой момент, когда лицо связывается с Компанией.
          </p>
          <p>
             &nbsp;
          </p>
          <p>
             Какие персональные данные собирает Компания:
          </p>
          <p>
             &nbsp;
          </p>
          <p>
             - имя, отчество и фамилия;
          </p>
          <p>
             &nbsp;
          </p>
          <p>
             - дата рождения;
          </p>
          <p>
             &nbsp;
          </p>
          <p>
             - пол, семейное положение;
          </p>
          <p>
             &nbsp;
          </p>
          <p>
             - почтовый адрес;
          </p>
          <p>
             &nbsp;
          </p>
          <p>
             - номер телефона;
          </p>
          <p>
             &nbsp;
          </p>
          <p>
             - адрес электронной почты.
          </p>
          <p>
             &nbsp;
          </p>
          <p>
             Персональные данные также могут включать дополнительные данные предоставляемые Пользователем по запросу Оператора/Компании в целях исполнения Оператором обязательств перед Пользователем, вытекающих из договора на оказание услуг. Оператор/Компания вправе запросить у пользователя документ, удостоверяющий личность Пользователя, а также иные дополнительные данные, которые по усмотрению Оператора/Компании будут являться необходимыми и достаточными для для идентификации такого Пользователя и позволяют исключить злоупотребления и нарушения прав третьих лиц.
          </p>
          <p>
             &nbsp;
          </p>
          <p>
             При обработке персональных данных нами обеспечивается точность персональных данных, их достаточность, а в необходимых случаях и актуальность по отношению к целям обработки персональных данных.
          </p>
          <p>
             &nbsp;
          </p>
          <p>
             3. Хранение и использование персональных данных
          </p>
          <p>
             &nbsp;
          </p>
          <p>
             Персональные данные Пользователя хранятся на электронных носителях и обрабатываются с использованием автоматизированных систем, за исключением случаев, когда не автоматизированная обработка персональных данных необходима в случаях исполнения требований законодательства.
          </p>
          <p>
             &nbsp;
          </p>
          <p>
             Также, Компания/Оператор может использовать персональные данные Пользователя для внутренних целей, таких как: проведение аудита, анализ данных и различных исследований в целях улучшения услуг Компании, а также взаимодействие с потребителем.
          </p>
          <p>
             &nbsp;
          </p>
          <p>
             4. Передача персональных данных
          </p>
          <p>
             &nbsp;
          </p>
          <p>
             Персональные данные Пользователей не передаются каким-либо третьим лицам, за исключением случаев, прямо предусмотренных настоящими Правилами.
          </p>
          <p>
             &nbsp;
          </p>
          <p>
             Обработка персональных данных Пользователя осуществляется без ограничения срока, любым законным способом, в том числе в информационных системах персональных данных с использованием средств автоматизации или без использования таких средств.
          </p>
          <p>
             &nbsp;
          </p>
          <p>
             Пользователь соглашается с тем, что Оператор вправе передовать персональные данные третьим лицам, в частности, курьерским службам, организациям почтовой связи, операторам электросвязи и т. д. исключительно для целей, указанных в разделе «Сбор персональных данных» настоящей Политики конфиденциальности.
          </p>
          <p>
             &nbsp;
          </p>
          <p>
             Персональные данные Пользователя могут быть переданы по запросам уполномоченных органов государственной власти РФ только по основаниям и в порядке , установленном законодательством Российской Федерации. Оператор осуществляет блокирование персональных данных, относящихся к соответствующему Пользователю, с момента обращения или запроса Пользователя или его представителя либо уполномоченного органа по защите прав субъектов персональных данных на период проверки, в случае выявления недостоверных персональных данных или неправомерных действий.
          </p>
          <p>
             &nbsp;
          </p>
          <p>
             Компания предоставляет третьим лицам минимальный объем персональных данных, необходимый только для оказания требуемой услуги или проведения необходимой транзакции.
          </p>
          <p>
             &nbsp;
          </p>
          <p>
             Персональная информация будет предоставляться Компанией только в целях обеспечения потребителей услугами, а также для улучшения этих услуг. Такая информация не будет предоставляться третьим лицам для их маркетинговых целей.
          </p>
          <p>
             &nbsp;
          </p>
          <p>
             Компания может раскрыть информацию /персональные данные о Вас, если Компания определит, что раскрытие необходимо или уместно в целях национальной безопасности, поддержания правопорядка или иных общественно важных случаях. Также Компания вправе раскрыть персональные данные/информацию в случае реорганизации, слияния или продажи Компания вправе передать любую или всю собираемую Компанией персональную информацию соответствующему третьему лицу.
          </p>
          <p>
             &nbsp;
          </p>
          <p>
             5. Уничтожение персональных данных
          </p>
          <p>
             &nbsp;
          </p>
          <p>
             Персональные данные пользователя уничтожаются при:
          </p>
          <p>
             &nbsp;
          </p>
          <p>
             - самостоятельном удалении Пользователем;
          </p>
          <p>
             &nbsp;
          </p>
          <p>
             - удаление Оператором информации, размещаемой Пользователем, а также
          </p>
          <p>
             &nbsp;
          </p>
          <p>
             - персональной страницы Пользователя;
          </p>
          <p>
             &nbsp;
          </p>
          <p>
             - при отзыве субъектом персональных данных согласия на обработку персональных
          </p>
          <p>
             &nbsp;
          </p>
          <p>
             - данных согласия на обработку персональных данных.
          </p>
          <p>
             &nbsp;
          </p>
          <p>
             6. Защита персональных данных
          </p>
          <p>
             &nbsp;
          </p>
          <p>
             Компания предпринимает меры предосторожности — включая правовые, организационные, административные, технические и физические — для обеспечения защиты Ваших персональных данных в соответствии со ст. 19 Федерального Закона от 27.07.2006 No 152 -ФЗ «О персональных данных» в целях обеспечения защиты персональных данных Пользователя от неправомерного или случайного доступа к ним, уничтожения или изменения, блокирования, копирования, распространения, а также от иных неправомерных действий третьих лиц.
          </p>
          <p>
             &nbsp;
          </p>
          <p>
             Компания не собирает персональные данные о несовершеннолетних. Если Компании станет известно о том, что Компания получила персональные данные о несовершеннолетнем, Компания предпримет меры для удаления такой информации в максимально короткий срок. Компания настоятельно рекомендует родителям и иным лицам, под чьим присмотром находятся несовершеннолетние (законные представители — родители, усыновители и попечители), контролировать использование несовершеннолетними веб-сайтов.
          </p>
          <p>
             &nbsp;
          </p>
          <p>
             Оператор не несет ответственности за действия третьих лиц, получивших в результате использования Интернета или Услуг Сайта доступ к информации о Пользователе и за последствия использования данных и информации, которые, в силу природы Сайта, доступны любому пользователю сети Интернет.
          </p>
          <p>
             &nbsp;
          </p>
          <p>
             Для того чтобы убедиться, что Ваши персональные данные находятся в безопасности, мы доводим нормы соблюдения конфиденциальности и безопасности до работников Компании и строго следим за исполнением мер соблюдения конфиденциальности внутри Компании.
          </p>
          <p>
             &nbsp;
          </p>
          <p>
             7. Обращения пользователей
          </p>
          <p>
             &nbsp;
          </p>
          <p>
             К настоящей Политике конфиденциальности и отношениям между Пользователем и Оператором применяется действующее законодательство Российской Федерации. Пользователи вправе направлять Оператору свои запросы, в том числе запросы относительно использования их персональных данных, направления отзыва согласия на обработку персональных данных по адресу, указанному в разделе Общие положения настоящего положения.
          </p>
          <p>
             &nbsp;
          </p>
          <p>
             Во всем остальном, что не отражено напрямую в Политике Конфиденциальности, Компания обязуется руководствоваться нормами и положениями Федерального закона от 27.07.2006 No 152-ФЗ «О персональных данных».
          </p>
          <p>
             &nbsp;
          </p>
          <p>
             Посетители сайта Компании, предоставляющий свои персональные данные и информацию, тем самым соглашается с положениями данной Политики Конфиденциальности.
          </p>
          <p>
             &nbsp;
          </p>
          <p>
             Компания оставляет за собой право вносить любые изменения в Политику в любое время по своему усмотрению с целью дальнейшего совершенствования системы защиты от несанкционированного доступа к сообщаемым Пользователями персональным данным без согласия Пользователя. Когда Компания вносит существенные изменения в Политику Конфиденциальности, на нашем сайте размещается соответствующее уведомление вместе с обновленной версией Политики Конфиденциальности.
          </p>
          <p>
             &nbsp;
          </p>
          <p>
             Действие настоящей Политики не распространяется на действия и итернет-ресурсы третьих лиц.
          </p>
          <p>
             &nbsp;
          </p>
          <p>
             ИП Гафарова А.А.
          </p>
        </div>
      </div>
    </div>

    <link rel="stylesheet" href="/fonts/stylesheet.css">

    <!-- Yandex.Metrika counter -->
    <script type="text/javascript" >
    (function (d, w, c) {
    (w[c] = w[c] || []).push(function() {
    try {
    w.yaCounter45562407 = new Ya.Metrika({
    id:45562407,
    clickmap:true,
    trackLinks:true,
    accurateTrackBounce:true,
    webvisor:true
    });
    } catch(e) { }
    });

    var n = d.getElementsByTagName("script")[0],
    s = d.createElement("script"),
    f = function () { n.parentNode.insertBefore(s, n); };
    s.type = "text/javascript";
    s.async = true;
    s.src = "https://mc.yandex.ru/metrika/watch.js";

    if (w.opera == "[object Opera]") {
    d.addEventListener("DOMContentLoaded", f, false);
    } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
    </script>
    <noscript><div><img src="https://mc.yandex.ru/watch/45562407" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
    <!-- /Yandex.Metrika counter -->

    <!-- Facebook Pixel Code -->
    <script>
    !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
    n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
    document,'script','https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '1740786036144475');
    fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=1740786036144475&ev=PageView&noscript=1"
    /></noscript>
    <!-- DO NOT MODIFY -->
    <!-- End Facebook Pixel Code -->
    <!-- VK Pixel Code -->
    <script type="text/javascript">(window.Image ? (new Image()) : document.createElement('img')).src = 'https://vk.com/rtrg?p=VK-RTRG-150134-fDfpt';</script>
    <!-- End VK Pixel Code -->

    <script type="text/javascript">
      window.initFacebookApi = function() {
        if (typeof FB === "undefined") {
          console.log('start init FB');
          window.fbAsyncInit = function() {
            FB.init({
              appId: '246327519222260',
              xfbml: true,
              version: 'v2.8'
            });
            FB.AppEvents.logPageView();
          };

          (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {
              return;
            }
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
          }(document, 'script', 'facebook-jssdk'));
        }
      }
    </script>


    <script src="/js/libs2.js<?php echo $last_edit;?>"></script>
    <script src="/js/build.js<?php echo $last_edit;?>"></script>

  </body>
</html>
