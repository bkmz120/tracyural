function transliterate(word){
  var a = {" ":"-","Ё":"YO","Й":"I","Ц":"TS","У":"U","К":"K","Е":"E","Н":"N","Г":"G","Ш":"SH","Щ":"SCH","З":"Z","Х":"H","Ъ":"-","ё":"yo","й":"i","ц":"ts","у":"u","к":"k","е":"e","н":"n","г":"g","ш":"sh","щ":"sch","з":"z","х":"h","ъ":"-","Ф":"F","Ы":"I","В":"V","А":"a","П":"P","Р":"R","О":"O","Л":"L","Д":"D","Ж":"ZH","Э":"E","ф":"f","ы":"i","в":"v","а":"a","п":"p","р":"r","о":"o","л":"l","д":"d","ж":"zh","э":"e","Я":"Ya","Ч":"CH","С":"S","М":"M","И":"I","Т":"T","Ь":"-","Б":"B","Ю":"YU","я":"ya","ч":"ch","с":"s","м":"m","и":"i","т":"t","ь":"-","б":"b","ю":"yu"};
  return word.split('').map(function (char) {
    return a[char] || char;
  }).join("");
}

$("input").keypress(function(event) {
    if (event.which == 13) {
        event.preventDefault();
    $(this).closest('form').addClass('totrigger');
        $(".totrigger .btn-send").trigger('click');
        $(".totrigger").removeClass('totrigger');
    }
});

$(document).on('click','.btn-send', function(event) {
  event.preventDefault();
  $(this).closest('form').addClass('inwork');
  var data = $('.inwork').serialize(),
    errors = 0;
  $('.inwork .required').each( function() {
    $(this).removeClass('animated shake');
    if($(this).val()=='') {
      $(this).attr('placeholder',$(this).data('err')).addClass('has-error');

      errors=1;
    } else {
      $(this).removeClass('has-error');
    }
  });
  if (errors==0) {
    $.post('/inc/sender.php', data, aftersend);
  } else {
    $('.inwork').removeClass('inwork');
  }
});

$(document).on('click','.close', function() {
  $.magnificPopup.close()
});

$(document).on('click','.open-popup', function() {
  var href = $(this).attr('href');
  if (href=="#formats-poopup") {
    var formatType = $(this).attr('format-type'),
        formatTypeText,
        formName;
    if (formatType=="standart") {
      formatTypeText = "<b>«Стандарт»</b>";
      formName = "Узнать цену Стандарт";
    }
    else if (formatType=="business") {
      formatTypeText = "<b>«Бизнес»</b>";
      formName = "Узнать цену Бизнес";
    }
    else if (formatType=="vip") {
      formatTypeText = "<b>«ВИП»</b>";
      formName = "Узнать цену ВИП";
    }
    else if (formatType=="platinum") {
      formatTypeText = "<b>«Платинум»</b>";
      formName = "Узнать цену Платинум";
    }

    $('.js-formats-poopup-type').html(formatTypeText);
    $('.js-formats-poopup-form-name').attr("value",formName);
  }

  if (href=="#for-who-mobile-popup") {
    var type = $(this).attr('for-who-type'),
        formName;
    if (type=="predprinimateli") {
      formName = "Форма для кого Предприниматели и владельцы бизнеса";
    }
    else if (type=="top-managers") {
      formName = "Форма для кого Топ-менеджеры";
    }
    else if (type=="managers") {
      formName = "Форма для кого Менеджеры";
    }
    else if (type=="freelancers") {
      formName = "Форма для кого Фрилансеры";
    }
    else if (type=="bussines-trainers") {
      formName = "Форма для кого Бизнес-тренеры";
    }
    else if (type=="success-peoples") {
      formName = "Форма для кого Все кто хочет достигать максимума";
    }

    $('.js-for-who-mobile-popup-formname').attr("value",formName);
    // предотвратить закрытие блока
    $(this).closest('.forwho__item').addClass('visible');
  }

  if (href=="#networkform-poopup") {
    var type = $(this).attr('networking-type'),
        formName;
    if (type=="whatisit") {
      formName = "Форма нетворкинг что это";
    }
    else if (type=="vip") {
      formName = "Форма нетворкинг vip";
    }
    else if (type=="business") {
      formName = "Форма нетворкинг бизнес";
    }

    $('.js-networking-popup-formname').attr("value",formName);
  }

  $.magnificPopup.open({
    items: {
      src: href,

    },
    showCloseBtn: true,
    type: 'inline',
    callbacks: {
        open: function() {
          $('body').addClass('popup-opened');
        },
        close: function() {
          $('body').removeClass('popup-opened');
          if ($(window).width()>767) {

          }
        }
        }
  }, 0);
  return false;
});

function aftersend(data){
  var step = $('.inwork input[name="step"]').val();
  if (step==='first') {
    var phone = $('.inwork input[name="phone"]').val();
    var form = $('.inwork input[name="form"]').val();
    var email = $('.inwork input[name="email"]').val();
    var type = $('.inwork input[name="type"]').val();

    if (type === 'full') {
      $('body').removeClass('side-opened');


      var postfix = 'full';
      var postfix_ga = 'Полная форма';

      if (typeof yaCounter45562407 !== 'undefined') {
        yaCounter45562407.reachGoal(transliterate(form).toLowerCase()+'-'+postfix,function(){
          yaCounter45562407.reachGoal('redirect-to-events',function(){
            window.location.href = "https://malik-space-events.timepad.ru/event/561382/";
          });
        });
      }

    } else {
      $.magnificPopup.open({
        items: {
          src: '#additional'
        },
        closeOnBgClick: false,
        showCloseBtn: false,
        type: 'inline',
        callbacks: {
          open: function() {
            $('body').addClass('popup-opened');
            //$('.mfp-bg').remove();

          },
          close: function() {
            $('body').removeClass('popup-opened');
            if ($(window).width()>767) {

            }

          }
          // e.t.c.
          }
      }, 0);

      $('#additional input[name="email"]').val(email);
      $('#additional input[name="phone"]').val(phone);
      $('#additional input[name="form"]').val(form);
      $('#additional input[name="id"]').val(data);
      var postfix = 'first';
      var postfix_ga = 'Только телефон';
      if (typeof yaCounter45562407 !== 'undefined') {
        yaCounter45562407.reachGoal(transliterate(form).toLowerCase()+'-'+postfix);
        //yaCounter.reachGoal('sent-form');
        //ga('send', 'event', 'Отправка формы', form, postfix_ga);
        console.log(transliterate(form).toLowerCase()+'-'+postfix);
      }
    }

    $('.fancybox-close').remove();
  } else if (step==='second') {
    var form = $('.inwork input[name="form"]').val();
    $('.inwork .btn-send').addClass('sent');
    $.magnificPopup.open({
      items: {
        src: '#message-sent',

      },
      showCloseBtn: false,
      type: 'inline',
      callbacks: {
          open: function() {
            $('body').addClass('popup-opened');
            //$('.mfp-bg').remove();

          },
          close: function() {
            $('body').removeClass('popup-opened');
            if ($(window).width()>767) {

            }
          }
          // e.t.c.
          }
    }, 0);
    setTimeout(function(){
      $.magnificPopup.close()
    }, 3000);
    var postfix = 'second';
    var postfix_ga = 'Доп.форма';
    if (typeof yaCounter45562407 !== 'undefined') {
      console.log(transliterate(form).toLowerCase()+'-'+postfix);
      yaCounter45562407.reachGoal(transliterate(form).toLowerCase()+'-'+postfix);
      yaCounter45562407.reachGoal('sent-form');
      //ga('send', 'event', 'Отправка формы', form, postfix_ga);
      //ga('send', 'event', 'Отправка формы', 'Любая', 'С емейлом');
      console.log(transliterate(form).toLowerCase()+'-'+postfix);
    }
  }

  $('.fancybox-close').remove();
  $('.inwork input[type="text"], .inwork textarea').val('');
  $('.inwork input[name="question"]').val('');
  $('.inwork').removeClass('inwork');


}