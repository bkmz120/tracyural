$(document).ready(function(){

  $(".books-slider").owlCarousel({
    items: 4,
    dots: false,
    loop: true,
    nav: true,
    //navText:["<img src='/images/arrow-left.svg'>","<img src='/images/arrow-right.svg'>"],
    responsive : {
      // breakpoint from 0 up
      0 : {
        dots: true,
        nav: false,
        items: 1,
      },
      // breakpoint from 768 up
      768 : {
        dots: false,
        nav: true,
        items: 3,
      },
      // breakpoint from 768 up
      992 : {
        dots: false,
        nav: true,
        items: 4,
      }
    }
  });
  
  $(".quotes-items").owlCarousel({
    items: 1,
    dots: true,
    loop: true,
    nav: false,
    //navText:["<img src='/images/arrow-left.svg'>","<img src='/images/arrow-right.svg'>"],
    responsive : {
      // breakpoint from 0 up
      0 : {

        items: 1,
      },
      // breakpoint from 768 up
      768 : {
        items: 1,
      },
      // breakpoint from 768 up
      992 : {
        items: 2,
      }
    }
  });


  //слайдер фото-галереи
  $(".photos-slider__items").owlCarousel({
    items: 5,
    dots: false,
    loop: true,
    autoplay:true,
    autoplayTimeout:2000,

    nav: false,
    responsive : {
      0 : {
        items: 2,
        autoplay:true,
        autoplayTimeout:2000,
      },
      560 : {
        items: 3,
        autoplay:true,
        autoplayTimeout:2000,
      },
      768 : {
        items: 3,
        autoplay:true,
        autoplayTimeout:2000,
      },
      992 : {
        items: 4,
        autoplay:true,
        autoplayTimeout:2000,
      },
      1260 : {
        items: 5,
        autoplay:true,
        autoplayTimeout:2000,
      },
      1400 : {
        items: 5,
        autoplay:true,
        autoplayTimeout:2000,
      },
      1645: {
        items:6,
        autoplay:true,
        autoplayTimeout:2000,
      }
    }
  }); 

  $(document).on('click', "header .header-menu li:not(.delimiter), .scroll-to", function(){
    var target = $(this).data('scroll');
    $('html, body').animate({
      scrollTop: $(target).offset().top-55
    }, 500);
  });

  //слайдер для мобильной версии блока с Сергеем Озеровым
  (function(){
    $owl = $('.sj-oz__box-items');
    var carousel_Settings = {
      dots: false,
      items:1,
      nav:false,
      //animateOut: 'fadeOut',
      smartSpeed:100,
    };
    var owlStatus = "";
    function initialize(){
      if( $( window ).width() <= 767 && owlStatus!="on") {
        // initialize owl-carousel if window screensize is less the 767px
        $owl.addClass('owl-carousel owl-theme');
        $owl.owlCarousel( carousel_Settings );
        owlStatus = "on";
      } 
      else if( $( window ).width() > 767 && owlStatus!="off") {
        // destroy owl-carousel and remove all depending classes if window screensize is bigger then 767px
        $owl.trigger('destroy.owl.carousel').removeClass('owl-carousel owl-loaded');
        $owl.find('.owl-stage-outer').children().unwrap();
        owlStatus = "off;";
      }
    }
    // initilize after window resize
    var id;
    $(window).resize( function() {
      clearTimeout(id);
      id = setTimeout(initialize, 500);
    });

    // initilize onload
    initialize();

    $('.sj-oz__box-items-arrows-prev').click(function(){
      $owl.trigger('prev.owl.carousel');
    });

    $('.sj-oz__box-items-arrows-next').click(function(){
      $owl.trigger('next.owl.carousel');
    });
  })();

  //раскрывающиеся блоки в "для кого мероприятие"
  (function(){

    $('.forwho__item').click(function(){
      if( $( window ).width() <= 767 ) {
        if (!$(this).hasClass('visible')) {
          $(this).addClass('visible');
        }
        else {  
          $(this).removeClass('visible');
        }
      }
      else {
        $(this).removeClass('visible');
      }      
    });
    
  })();

  //раскрывающиеся блоки в мобильной версии "форматы участия в тренинги"
  (function(){
    $('.js-formsInvolv-mob-item-head').click(function(){ 
    	var $item = $(this).parent('.js-ormsInvolv-mob-item');
      if (!$item.hasClass('visible')) {
        $item.addClass('visible');
      }
      else {
        $item.removeClass('visible');
      }
    });
  })();

  //слайдер "мероприятия которые мы орагнизовали"
  (function(){    
    var $currentNum = $('.js-measure-current-num'),
        $totalNum = $('.js-measure-total-num');
    
    $(".measures-ms__items").owlCarousel({
      items: 1,
      dots: false,
      loop: false,
      autoplay:false,
      autoplayTimeout:2000,
      nav: true,
      onChanged:changeCurrentNum,
      onInitialized:changeTotalNum,      
    }); 

    function changeCurrentNum(e) {
      $currentNum.text(e.item.index+1);
    }

    function changeTotalNum(e) {
      $totalNum.text("/ "+ e.item.count);
    } 

  })();


});